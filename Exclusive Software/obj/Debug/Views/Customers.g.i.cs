﻿#pragma checksum "..\..\..\Views\Customers.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "4C77DC8A2667B4BE9951A4FCBB094464"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Exclusive_Software.Views {
    
    
    /// <summary>
    /// Customers
    /// </summary>
    public partial class Customers : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 8 "..\..\..\Views\Customers.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid LayoutRoot;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\..\Views\Customers.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid headRow;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\..\Views\Customers.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid HeadTitulo;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\..\Views\Customers.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel ContentTituloPanel;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\..\Views\Customers.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock ApplicationNameTextBlock;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\..\Views\Customers.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid buttonHead;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\..\Views\Customers.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAdd;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\..\Views\Customers.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid bodyRow;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\..\Views\Customers.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ScrollViewer PageScrollViewer;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\..\Views\Customers.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel ContentStackPanel;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\..\Views\Customers.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dgCustomers;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Exclusive Software;component/views/customers.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Views\Customers.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.LayoutRoot = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.headRow = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.HeadTitulo = ((System.Windows.Controls.Grid)(target));
            return;
            case 4:
            this.ContentTituloPanel = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 5:
            this.ApplicationNameTextBlock = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 6:
            this.buttonHead = ((System.Windows.Controls.Grid)(target));
            return;
            case 7:
            this.btnAdd = ((System.Windows.Controls.Button)(target));
            
            #line 31 "..\..\..\Views\Customers.xaml"
            this.btnAdd.Click += new System.Windows.RoutedEventHandler(this.btnAdd_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.bodyRow = ((System.Windows.Controls.Grid)(target));
            return;
            case 9:
            this.PageScrollViewer = ((System.Windows.Controls.ScrollViewer)(target));
            return;
            case 10:
            this.ContentStackPanel = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 11:
            this.dgCustomers = ((System.Windows.Controls.DataGrid)(target));
            
            #line 59 "..\..\..\Views\Customers.xaml"
            this.dgCustomers.PreviewKeyDown += new System.Windows.Input.KeyEventHandler(this.dgCustomers_PreviewKeyDown);
            
            #line default
            #line hidden
            
            #line 60 "..\..\..\Views\Customers.xaml"
            this.dgCustomers.MouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(this.dgCustomers_MouseDoubleClick);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

