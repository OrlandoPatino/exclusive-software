﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Exclusive_Software.Model;

namespace Exclusive_Software.Services
{
    /// <summary>
    /// Poll Service interface
    /// </summary>
    public partial interface IPollService
    {
        /// <summary>
        /// Gets a Poll
        /// </summary>
        /// <param name="pollID">poll identifier</param>
        /// <returns>A poll</returns>
        Poll GetPollById(int pollID);

        /// <summary>
        /// Gets a PollQuestion
        /// </summary>
        /// <param name="pollID">pollQuestion identifier</param>
        /// <returns>A poll Question</returns>
        PollQuestion GetPollQuestionById(int pollQuestionID);

        /// <summary>
        /// Gets a PollAnswer
        /// </summary>
        /// <param name="pollID">PollAnswer identifier</param>
        /// <returns>A poll Answer</returns>
        PollAnswer GetPollAnswerById(int pollAnswerID);

        /// <summary>
        /// Marks poll as deleted
        /// </summary>
        /// <param name="pollId">Poll identifier</param>
        void MarkPollAsDeleted(int pollId);

        /// <summary>
        /// Gets all polls
        /// </summary>
        /// <returns>A poll list</returns>
        List<Poll> GetAllPolls();

        /// <summary>
        /// Gets a pollAnswer List by PollQuestionID
        /// </summary>
        /// <param name="PollQuestionID">PollQuestionID</param>
        /// <returns>A poll</returns>
        List<PollAnswer> GetPollAnswersByPollQuestionID(int pollQuestionID);

        /// <summary>
        /// Gets a PollQuestion List by PollID
        /// </summary>
        /// <param name="PollID">PollID</param>
        /// <returns>A poll Question List</returns>
        List<PollQuestion> GetPollQuestionByPollID(int PollID);

        /// <summary>
        /// Gets a poll Answers
        /// </summary>
        /// <param name="Name">Poll Name</param>
        /// <returns>A poll</returns>
        List<Poll> GetPollByName(string Name);

        /// <summary>
        /// Updates the poll
        /// </summary>
        /// <param name="poll">Poll</param>
        void UpdatePoll(Poll poll);

        /// <summary>
        /// Updates the poll
        /// </summary>
        /// <param name="pollQuestion">pollQuestion</param>
        void UpdatePollQuestion(PollQuestion pollQuestion);


        /// <summary>
        /// Updates the pollAnswer
        /// </summary>
        /// <param name="pollAnswer">pollAnswer</param>
        void UpdatePollAnswer(PollAnswer pollAnswer);

        /// <summary>
        /// Inserts a Poll
        /// </summary>
        /// <param name="poll">Poll</param>
        void InsertPoll(Poll poll);

        /// <summary>
        /// Inserts a Poll
        /// </summary>
        /// <param name="poll">Poll</param>
        void InsertPollQuestion(PollQuestion pollQuestion);

        /// <summary>
        /// Inserts a Poll Answer
        /// </summary>
        /// <param name="pollAnswer">pollAnswer</param>
        void InsertPollAnswer(PollAnswer pollAnswer);

        /// <summary>
        /// Deletes a poll answer
        /// </summary>
        /// <param name="pollAnswerId">Poll answer identifier</param>
        void DeletePollAnswer(int pollAnswerId);

        /// <summary>
        /// Deletes a poll Question
        /// </summary>
        /// <param name="pollAnswerId">Poll Question identifier</param>
        void DeletePollQuestion(int pollQuestionId);

        /// <summary>
        /// Gets a poll Answers
        /// </summary>
        /// <param name="Name">Poll Name</param>
        /// <returns>A poll</returns>
        Poll GetPollByEventID(int eventID);

        /// <summary>
        /// Inserts a Poll Answer
        /// </summary>
        /// <param name="pollAnswer">pollAnswer</param>
        void InsertPollVotingRecord(int pollAnswerID, int customerID);

        /// <summary>
        /// Checks If an anwers was Answered by a customer
        /// </summary>
        /// <param name="PollQuestionID">pollAnswerID</param>
        /// <param name="customerID">customerID</param>
        /// <returns>boll</returns>
        bool CheckIfAnswered(int pollAnswerID, int customerID);

        /// <summary>
        /// Deletes a Poll Voting Record
        /// </summary>
        /// <param name="pollAnswerId">Poll Question identifier</param>
        void DeletePollVotingRecord(int pollVotingRecordID);

        /// <summary>
        /// GetPollVotingRecord
        /// </summary>
        /// <param name="PollQuestionID">pollAnswerID</param>
        /// <param name="customerID">customerID</param>
        /// <returns>PollVotingRecord</returns>
        PollVotingRecord GetPollVotingRecord(int pollVotingRecordID);

        /// <summary>
        /// Get Poll Voting Record By Answer and Customer
        /// </summary>
        /// <param name="PollQuestionID">pollAnswerID</param>
        /// <param name="customerID">customerID</param>
        /// <returns>boll</returns>
        PollVotingRecord GetPollVotingRecordByAnswerandCustomer(int pollAnswerID, int customerID);

        /// <summary>
        /// Indicate wheter can modify a poll
        /// </summary>
        /// <param name="pollID">pollQuestion identifier</param>
        /// <returns>bool</returns>
        bool CanModifyPoll(int pollID);

    }
}
