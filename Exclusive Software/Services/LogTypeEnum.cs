namespace Exclusive_Software.Services
{
    /// <summary>
    /// Represents a log item type (need to be synchronize with [Nop_LogType] table)
    /// </summary>
    public enum LogTypeEnum : int
    {
        /// <summary>
        /// Error in WindowsLogin Page
        /// </summary>
        WindowsLogin = 1,
        /// <summary>
        /// Error in WindowsMain Page
        /// </summary>
        WindowsMain = 2,
        /// <summary>
        /// Error in CustomerRegister Control
        /// </summary>
        CustomerRegister = 3,
        /// <summary>
        /// Error in Customers Control
        /// </summary>
        Customers = 4,
        /// <summary>
        /// Error in EventAccess Control
        /// </summary>
        EventAccess = 5,
        /// <summary>
        /// Error in EventPoll Control
        /// </summary>
        EventPoll = 6,
        /// <summary>
        /// Error in EventPollQuestion Control
        /// </summary>
        EventPollQuestion = 7,
        /// <summary>
        /// Error in EventRegister Control
        /// </summary>
        EventRegister = 8,
        /// <summary>
        /// Error in Events Control
        /// </summary>
        Events = 9,
        /// <summary>
        /// Error in ManagePoll Control
        /// </summary>
        ManagePoll = 10,
        /// <summary>
        /// Error in PollQuestionView Control
        /// </summary>
        PollQuestionView = 11,
        /// <summary>
        /// Error in Polls Control
        /// </summary>
        Polls = 12,
        /// <summary>
        /// Error in Unknown Control or Page
        /// </summary>
        Unknown = 20,
    }
}
