﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Exclusive_Software.Model;

namespace Exclusive_Software.Services
{
    /// <summary>
    /// Customer service interface
    /// </summary>
    public partial interface ICustomerService
    {
        /// <summary>
        /// Gets a customer
        /// </summary>
        /// <param name="customerId">Customer identifier</param>
        /// <returns>A customer</returns>
        Customer GetCustomerById(int customerId);

        /// <summary>
        /// Get Customer by CI
        /// </summary>
        /// <param name="CI">Identity Card</param>
        /// <returns>A boolean</returns>        
        Customer GetCustomerbyCI(string CI);

        /// <summary>
        /// Marks customer as deleted
        /// </summary>
        /// <param name="customerId">Customer identifier</param>
        void MarkCustomerAsDeleted(int customerId);

        /// <summary>
        /// Gets a customer by email
        /// </summary>
        /// <param name="email">Customer Email</param>
        /// <returns>A customer</returns>
        Customer GetCustomerByEmail(string email);

        /// <summary>
        /// Gets all customers
        /// </summary>
        /// <returns>A customers list</returns>
        List<Customer> GetAllCustomer();

        /// <summary>
        /// Gets a customer by Name
        /// </summary>
        /// <param name="email">Customer Name</param>
        /// <returns>A List of customers</returns>
        List<Customer> GetCustomerByName(string Name);

        /// <summary>
        /// Gets a customer by Name or CI
        /// </summary>
        /// <param name="search">Customer Name or CI</param>
        /// <returns>A List of customers</returns>
        List<Customer> GetCustomerByNameOrCI(string search);

        /// <summary>
        /// Updates the customer
        /// </summary>
        /// <param name="customer">Customer</param>
        void UpdateCustomer(Customer customer);

        /// <summary>
        /// Inserts a customer
        /// </summary>
        /// <param name="customerSession">Customer</param>
        void InsertCustomer(Customer customer);

        /// <summary>
        /// Gets all customers Roles
        /// </summary>
        /// <returns>A customers list</returns>
        List<CustomerRole> GetCustomerRoles();

        /// <summary>
        /// Gets all customers Role by CustomerID
        /// </summary>
        /// <returns>the customer role</returns>
        CustomerRole GetCustomerRoleByCustomerID(int customerID);

        /// <summary>
        /// Gets a customer by its password
        /// </summary>
        /// <param name="Password">Password</param>
        /// <returns>A customer</returns>
        Customer GetCustomerByPassword(string Password);

    }
}
