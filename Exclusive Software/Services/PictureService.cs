using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using Exclusive_Software.Model;
using Exclusive_Software.Helpers;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Exclusive_Software.Services
{
    /// <summary>
    /// Picture service
    /// </summary>
    public partial class PictureService : IPictureService
    {
        #region Fields

        /// <summary>
        /// Object context
        /// </summary>
        private readonly ExclusiveDBEntities _context;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="context">Object context</param>
        public PictureService()
        {
            this._context = new ExclusiveDBEntities();
        }

        #endregion

        #region Methods


        /// <summary>
        /// Gets a picture
        /// </summary>
        /// <param name="pictureId">Picture identifier</param>
        /// <returns>Picture</returns>
        public Picture GetPictureById(int pictureId)
        {
            if (pictureId == 0)
                return null;

            var query = from p in _context.Pictures
                        where p.PictureID == pictureId
                        select p;

            var picture = query.SingleOrDefault();

            return picture;
        }

        /// <summary>
        /// Deletes a picture
        /// </summary>
        /// <param name="pictureId">Picture identifier</param>
        public void DeletePicture(int pictureId)
        {
            var picture = GetPictureById(pictureId);
            if (picture == null)
                return;

            //delete from database
            _context.Pictures.Remove(picture);
            _context.SaveChanges();

        }
        
        /// <summary>
        /// Gets a collection of pictures
        /// </summary>
        /// <param name="pageIndex">Current page</param>
        /// <param name="pageSize">Items on each page</param>
        /// <returns>Paged list of pictures</returns>
        public List<Picture> GetAllPictures()
        {
            var query = from p in _context.Pictures
                       orderby p.PictureID descending
                       select p;
            var pics = query.ToList(); 
            return pics;
        }
        

        /// <summary>
        /// Inserts a picture
        /// </summary>
        /// <param name="pictureBinary">The picture binary</param>
        /// <param name="mimeType">The picture MIME type</param>
        /// <param name="isNew">A value indicating whether the picture is new</param>
        /// <returns>Picture</returns>
        public Picture InsertPicture(byte[] pictureBinary, string mimeType, bool isNew)
        {
            mimeType = CommonHelper.EnsureNotNull(mimeType);
            mimeType = CommonHelper.EnsureMaximumLength(mimeType, 20);

            var picture = new Picture();
            picture.PictureBinary = pictureBinary;
            picture.MimeType = mimeType;
            picture.IsNew = isNew;

            _context.Pictures.Add(picture);
            _context.SaveChanges();

            return picture;
        }

        /// <summary>
        /// Updates the picture
        /// </summary>
        /// <param name="pictureId">The picture identifier</param>
        /// <param name="pictureBinary">The picture binary</param>
        /// <param name="mimeType">The picture MIME type</param>
        /// <param name="isNew">A value indicating whether the picture is new</param>
        /// <returns>Picture</returns>
        public Picture UpdatePicture(Picture picture)
        {
            if (picture == null)
                return null;

            picture.MimeType = CommonHelper.EnsureNotNull(picture.MimeType);
            picture.MimeType = CommonHelper.EnsureMaximumLength(picture.MimeType, 20);

            _context.SaveChanges();

            return picture;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets a local thumb image path
        /// </summary>
        public string LocalThumbImagePath
        {
            get
            {
                string path = System.IO.Directory.GetCurrentDirectory() + "Resources\\thumbs";
                return path;
            }
        }

        /// <summary>
        /// Gets the local image path
        /// </summary>
        public string LocalImagePath
        {
            get
            {
                string path = System.IO.Directory.GetCurrentDirectory() + "Resources";
                return path;
            }
        }

        /// <summary>
        /// BitmapSourceToByteArray
        /// </summary>
        /// <param name="image">TBitmapSource</param>
        /// <returns>byte[]</returns>
        public byte[] BitmapSourceToByteArray(BitmapSource image)
        {
            using (var stream = new MemoryStream())
            {
                var encoder = new PngBitmapEncoder(); // or some other encoder
                encoder.Frames.Add(BitmapFrame.Create(image));
                encoder.Save(stream);
                return stream.ToArray();
            }
        }

        /// <summary>
        /// ByteToBitmapSource
        /// </summary>
        /// <param name="byte[] image">byte[] image</param>
        /// <returns>BitmapSource</returns>
        public BitmapSource ByteToBitmapSource(byte[] image)
        {
            BitmapImage imageSource = new BitmapImage();
            using (MemoryStream stream = new MemoryStream(image))
            {
                stream.Seek(0, SeekOrigin.Begin);
                imageSource.BeginInit();
                imageSource.StreamSource = stream;
                imageSource.CacheOption = BitmapCacheOption.OnLoad;
                imageSource.EndInit();
            }
            return imageSource;
        }

        #endregion
    }
}
