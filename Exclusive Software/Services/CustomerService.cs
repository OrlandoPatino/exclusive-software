﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Exclusive_Software.Model;
using Exclusive_Software.Helpers;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data;
using System.Data.Objects;

namespace Exclusive_Software.Services
{
    public partial class CustomerService : ICustomerService
    {
        #region Fields

        /// <summary>
        /// Object context
        /// </summary>
        private readonly ExclusiveDBEntities _context;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="context">Object context</param>
        public CustomerService()
        {
            this._context = new ExclusiveDBEntities();
        }

        #endregion

        #region Utilities

        /// <summary>
        /// Gets a customer
        /// </summary>
        /// <param name="customerId">Customer identifier</param>
        /// <returns>A customer</returns>
        public Customer GetCustomerById(int customerId)
        {
            ///Change test
            if (customerId == 0)
                return null;

            var query = from c in _context.Customers
                        where c.CustomerID == customerId && !c.Deleted
                        select c;
            var customer = query.SingleOrDefault();

            return customer;

        }

        /// <summary>
        /// Get Customer by CI
        /// </summary>
        /// <param name="CI">Identity Card</param>
        /// <returns>A boolean</returns>        
        public Customer GetCustomerbyCI(string CI)
        {
            var query = from c in _context.Customers
                        where c.CI == CI && !c.Deleted
                        select c;

            Customer customer = query.FirstOrDefault();

            return customer;
        }

        /// <summary>
        /// Marks customer as deleted
        /// </summary>
        /// <param name="customerId">Customer identifier</param>
        public void MarkCustomerAsDeleted(int customerId)
        {
            var customer = GetCustomerById(customerId);
            if (customer != null)
            {
                customer.Deleted = true;
                UpdateCustomer(customer);
            }
        }

        /// <summary>
        /// Gets a customer by email
        /// </summary>
        /// <param name="email">Customer Email</param>
        /// <returns>A customer</returns>
        public Customer GetCustomerByEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
                return null;

            var query = from c in _context.Customers
                        orderby c.CustomerID
                        where c.Email == email && !c.Deleted
                        select c;
            var customer = query.FirstOrDefault();
            return customer;
        }

        /// <summary>
        /// Gets all customers
        /// </summary>
        /// <returns>A customers list</returns>
        public List<Customer> GetAllCustomer()
        {

            var query = from c in _context.Customers
                        where !c.Deleted
                        orderby c.FirstName
                        select c;

            var customers = query.ToList();
            return customers;
        }

        /// <summary>
        /// Gets a customer by Name
        /// </summary>
        /// <param name="email">Customer Name</param>
        /// <returns>A customer</returns>
        public List<Customer> GetCustomerByName(string Name)
        {

            var query = from c in _context.Customers
                        where (c.FirstName.Contains(Name) || c.LastName.Contains(Name)) && !c.Deleted
                        orderby c.FirstName
                        select c;
            var customers = query.ToList();
            return customers;
        }

        /// <summary>
        /// Gets a customer by Name or CI
        /// </summary>
        /// <param name="search">Customer Name or CI</param>
        /// <returns>A List of customers</returns>
        public List<Customer> GetCustomerByNameOrCI(string search)
        {

            var query = from c in _context.Customers
                        where (c.FirstName.Contains(search) || c.LastName.Contains(search) || c.CI.Contains(search)) && !c.Deleted
                        orderby c.FirstName
                        select c;
            var customers = query.ToList();
            return customers;
        }

        /// <summary>
        /// Updates the customer
        /// </summary>
        /// <param name="customer">Customer</param>
        public void UpdateCustomer(Customer customer)
        {
            if (customer == null)
                throw new ArgumentNullException("UpdateCustomer");

            customer.CI = CommonHelper.EnsureNotNull(customer.CI);
            customer.CI = customer.CI.Trim();
            customer.CI = CommonHelper.EnsureMaximumLength(customer.CI, 50);
            customer.FirstName = CommonHelper.EnsureNotNull(customer.FirstName);
            customer.FirstName = CommonHelper.EnsureMaximumLength(customer.FirstName, 255);
            customer.FirstName = customer.FirstName.ToUpper();
            customer.LastName = CommonHelper.EnsureNotNull(customer.LastName);
            customer.LastName = CommonHelper.EnsureMaximumLength(customer.LastName, 255);
            customer.LastName = customer.LastName.ToUpper();
            customer.Gender = CommonHelper.EnsureNotNull(customer.Gender);
            customer.Email = CommonHelper.EnsureNotNull(customer.Email);
            customer.Email = customer.Email.Trim();
            customer.Email = customer.Email.ToLower();
            customer.Email = CommonHelper.EnsureMaximumLength(customer.Email, 255);
            customer.PhoneNumber = CommonHelper.EnsureNotNull(customer.PhoneNumber);
            customer.PhoneNumber = CommonHelper.EnsureMaximumLength(customer.PhoneNumber, 50);
            customer.Instagram = CommonHelper.EnsureNotNull(customer.Instagram);
            customer.Instagram = CommonHelper.EnsureMaximumLength(customer.Instagram, 50);
            customer.Twitter = CommonHelper.EnsureNotNull(customer.Twitter);
            customer.Twitter = CommonHelper.EnsureMaximumLength(customer.Twitter, 50);
            customer.Comments = CommonHelper.EnsureNotNull(customer.Comments);
            customer.PasswordHash = CommonHelper.EnsureNotNull(customer.PasswordHash);
            customer.PasswordHash = CommonHelper.EnsureMaximumLength(customer.PasswordHash, 255);

            _context.SaveChanges();

        }

        /// <summary>
        /// Inserts a customer
        /// </summary>
        /// <param name="customerSession">Customer</param>
        public void InsertCustomer(Customer customer)
        {
            if (customer == null)
                throw new ArgumentNullException("InsertCustomer");

            customer.CI = CommonHelper.EnsureNotNull(customer.CI);
            customer.CI = customer.CI.Trim();
            customer.CI = CommonHelper.EnsureMaximumLength(customer.CI, 50);
            customer.FirstName = CommonHelper.EnsureNotNull(customer.FirstName);
            customer.FirstName = CommonHelper.EnsureMaximumLength(customer.FirstName, 255);
            customer.LastName = CommonHelper.EnsureNotNull(customer.LastName);
            customer.LastName = CommonHelper.EnsureMaximumLength(customer.LastName, 255);
            customer.Gender = CommonHelper.EnsureNotNull(customer.Gender);
            customer.Email = CommonHelper.EnsureNotNull(customer.Email);
            customer.Email = customer.Email.Trim();
            customer.Email = CommonHelper.EnsureMaximumLength(customer.Email, 255);
            customer.PhoneNumber = CommonHelper.EnsureNotNull(customer.PhoneNumber);
            customer.PhoneNumber = CommonHelper.EnsureMaximumLength(customer.PhoneNumber, 50);
            customer.Instagram = CommonHelper.EnsureNotNull(customer.Instagram);
            customer.Instagram = CommonHelper.EnsureMaximumLength(customer.Instagram, 50);
            customer.Twitter = CommonHelper.EnsureNotNull(customer.Twitter);
            customer.Twitter = CommonHelper.EnsureMaximumLength(customer.Twitter, 50);
            customer.Comments = CommonHelper.EnsureNotNull(customer.Comments);
            customer.PasswordHash = CommonHelper.EnsureNotNull(customer.PasswordHash);
            customer.PasswordHash = CommonHelper.EnsureMaximumLength(customer.PasswordHash, 255);

            _context.Customers.Add(customer);
            _context.SaveChanges();
        }

        /// <summary>
        /// Gets all customers Roles
        /// </summary>
        /// <returns>A customers list</returns>
        public List<CustomerRole> GetCustomerRoles()
        {

            var query = from cr in _context.CustomerRoles
                        where !cr.Deleted
                        orderby cr.Name
                        select cr;

            var customerRoles = query.ToList();
            return customerRoles;
        }

        /// <summary>
        /// Gets wheter a Customer Attend To Event
        /// </summary>
        /// <param name="curtomerID">curtomerID</param>
        /// <param name="eventID">eventID</param>
        /// <returns>boolean</returns>
        public bool CustomerAttendToEvent(int curtomerID, int eventID)
        {
            EventService eventService = new EventService();
            bool attend = false;

            Customer customer = GetCustomerById(curtomerID);
            if (customer != null)
            {
                GuestList gl = eventService.GetGuestListBycustomerIdAndEventID(customer.CustomerID, eventID);
                if (gl != null)
                {
                    attend = true;
                }
            }

            return attend;
        }


        /// <summary>
        /// Gets or sets the current user
        /// </summary>
        public Customer User
        {
            get
            {
                return Exclusive_Software.App.Current.Resources["UserInfo"] as Customer;
            }
            set
            {
                Exclusive_Software.App.Current.Resources["UserInfo"] = value;
            }
        }


        /// <summary>
        /// Gets all customers Role by CustomerID
        /// </summary>
        /// <returns>the customer role</returns>
        public CustomerRole GetCustomerRoleByCustomerID(int customerID)
        {

            var query = from c in _context.Customers
                        join cr in _context.CustomerRoles on c.CustomerRoleID equals cr.CustomerRoleID
                        where !cr.Deleted && !c.Deleted && c.CustomerID == customerID
                        select cr;

            var customerRole = query.SingleOrDefault();
            return customerRole;
        }

        /// <summary>
        /// Gets a customer by its password
        /// </summary>
        /// <param name="Password">Password</param>
        /// <returns>A customer</returns>
        public Customer GetCustomerByPassword(string Password)
        {
            var query = from c in _context.Customers
                        where c.PasswordHash == Password && !c.Deleted
                        select c;

            var customer = query.FirstOrDefault();

            return customer;

        }

        #endregion
    }
}
