﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Exclusive_Software.Model;
using Exclusive_Software.Helpers;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data;
using System.Data.Objects;

namespace Exclusive_Software.Services
{
    public partial class EventService : IEventService
    {

        #region Fields

        /// <summary>
        /// Object context
        /// </summary>
        private readonly ExclusiveDBEntities _context;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="context">Object context</param>
        public EventService()
        {
            this._context = new ExclusiveDBEntities();
        }

        #endregion

        #region Utilities

        /// <summary>
        /// Gets an Event
        /// </summary>
        /// <param name="EventId">Event identifier</param>
        /// <returns>An event</returns>
        public Event GetEventById(int eventId)
        {
            if (eventId == 0)
                return null;

            var query = from e in _context.Events
                        where e.EventID == eventId
                        select e;

            var _event = query.SingleOrDefault();

            return _event;

        }


        /// <summary>
        /// Marks event as deleted
        /// </summary>
        /// <param name="eventId">Event identifier</param>
        public void MarkEventAsDeleted(int eventId)
        {
            var Event = GetEventById(eventId);
            if (Event != null)
            {
                Event.Deleted = true;
                UpdateEvent(Event);
            }
        }

        /// <summary>
        /// Gets all events
        /// </summary>
        /// <returns>A event list</returns>
        public List<Event> GetAllEvents()
        {
            var query = from e in _context.Events
                        where !e.Deleted
                        select e;

            var _event = query.ToList();
            return _event;
        }

        /// <summary>
        /// Gets all pending events
        /// </summary>
        /// <returns>A event list</returns>
        public List<Event> GetAllPendingEvents()
        {
            DateTime today = DateTime.Now;
            DateTime yesterday = today.AddDays(-1);
            var query = from e in _context.Events
                        where !e.Deleted && e.Time >= yesterday ///le resto dias para tener una olgura para la cerrada del evento
                        select e;

            var _event = query.ToList();
            return _event;
        }

        /// <summary>
        /// Gets all Assisted events
        /// </summary>
        /// <returns>A event list</returns>
        public List<Event> GetAllAssistedEvents(int customerID)
        {
            var query = from e in _context.Events
                        join gl in _context.GuestLists on e.EventID equals gl.EventID
                        where gl.Attended == true && !e.Deleted && gl.CustomerID == customerID
                        select e;

            var _events = query.ToList();
            return _events;
        }


        /// <summary>
        /// Gets an event by Name
        /// </summary>
        /// <param name="name">Event Name</param>
        /// <returns>A customer</returns>
        public List<Event> GetEventByName(string Name)
        {

            var query = from e in _context.Events
                        orderby e.EventID
                        where e.Name.Contains(Name)
                        select e;
            var _events = query.ToList();
            return _events;
        }

        /// <summary>
        /// Updates the Event
        /// </summary>
        /// <param name="customer">Event</param>
        public void UpdateEvent(Event _event)
        {
            if (_event == null)
                throw new ArgumentNullException("UpdateEvent");

            _event.Name = CommonHelper.EnsureNotNull(_event.Name);
            _event.Name = CommonHelper.EnsureMaximumLength(_event.Name, 255);
            _event.Location = CommonHelper.EnsureNotNull(_event.Location);

            _context.SaveChanges();
        }

        /// <summary>
        /// Inserts an Event
        /// </summary>
        /// <param name="Event">Event</param>
        public void InsertEvent(Event _event)
        {
            if (_event == null)
                throw new ArgumentNullException("InsertCustomer");

            _event.Name = CommonHelper.EnsureNotNull(_event.Name);
            _event.Name = CommonHelper.EnsureMaximumLength(_event.Name, 255);
            _event.Location = CommonHelper.EnsureNotNull(_event.Location);

            _context.Events.Add(_event);
            _context.SaveChanges();
        }

        /// <summary>
        /// Gets an GuestList
        /// </summary>
        /// <param name="guestList">guestList identifier</param>
        /// <returns>An event</returns>
        public GuestList GetGuestListById(int guestListID)
        {
            if (guestListID == 0)
                return null;

            var query = from gl in _context.GuestLists
                        where gl.GuestListID == guestListID
                        select gl;

            var _guestList = query.SingleOrDefault();

            return _guestList;
        }

        /// <summary>
        /// Gets an GuestList
        /// </summary>
        /// <param name="eventID">event identifier</param>
        /// <returns>An event</returns>
        public List<GuestList> GetGuestListByEventId(int eventID)
        {
            if (eventID == 0)
                return null;

            var query = from gl in _context.GuestLists
                        join c in _context.Customers on gl.CustomerID equals c.CustomerID
                        where gl.EventID == eventID && !c.Deleted
                        select gl;

            var _guestLists = query.ToList();
            return _guestLists;
        }

        /// <summary>
        /// Gets an GuestList
        /// </summary>
        /// <param name="guestList">customer identifier</param>
        /// <returns>list of GuestList</returns>
        public List<GuestList> GetGuestListBycustomerId(int customerID)
        {
            if (customerID == 0)
                return null;

            var query = from gl in _context.GuestLists
                        join c in _context.Customers on gl.CustomerID equals c.CustomerID
                        where gl.CustomerID == customerID && !c.Deleted
                        select gl;

            var _guestLists = query.ToList();
            return _guestLists;
        }


        /// <summary>
        /// Gets an GuestList
        /// </summary>
        /// <param name="guestList">customer identifier</param>
        /// <returns>list of GuestList</returns>
        public bool IsGuestListAsociated(int customerID, int eventID)
        {
            if (customerID == 0)
                return false;

            if (eventID == 0)
                return false;

            var query = from gl in _context.GuestLists
                        join c in _context.Customers on gl.CustomerID equals c.CustomerID
                        where gl.CustomerID == customerID && gl.EventID == eventID && !c.Deleted
                        select gl;

            var _guestList = query.SingleOrDefault();
            if (_guestList != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Gets an GuestList
        /// </summary>
        /// <param name="guestList">customer identifier</param>
        /// <returns>list of GuestList</returns>
        public GuestList GetGuestListBycustomerIdAndEventID(int customerID, int eventID)
        {
            if (customerID == 0)
                return null;

            if (eventID == 0)
                return null;

            var query = from gl in _context.GuestLists
                        join c in _context.Customers on gl.CustomerID equals c.CustomerID
                        where gl.CustomerID == customerID && gl.EventID == eventID && !c.Deleted
                        select gl;

            var _guestList = query.SingleOrDefault();
            return _guestList;
        }

        /// <summary>
        /// Deletes a GuestList
        /// </summary>
        /// <param name="guestListID">gues tList ID identifier</param>
        public void DeleteGuestList(int guestListID)
        {
            var guestList = GetGuestListById(guestListID);
            if (guestList == null)
                return;

            _context.GuestLists.Remove(guestList);
            _context.SaveChanges();

        }

        /// <summary>
        /// Inserts a GuestList
        /// </summary>
        /// <param name="Event">Event</param>
        public void InsertGuestList(GuestList _guestList)
        {
            if (_guestList == null)
                throw new ArgumentNullException("InsertGuestList");

            _context.GuestLists.Add(_guestList);
            _context.SaveChanges();
        }

        /// <summary>
        /// Updates the GuestList
        /// </summary>
        /// <param name="_guestList">guestList</param>
        public void UpdateGuestList(GuestList _guestList)
        {
            if (_guestList == null)
                throw new ArgumentNullException("UpdateGuestList");
            _context.SaveChanges();

        }

        /// <summary>
        /// Gets an GuestList
        /// </summary>
        /// <param name="eventID">event identifier</param>
        /// <returns>An event</returns>
        public List<GuestList> GetGuestListByEventIdandCustomerName(int eventID, string customerSearch)
        {
            if (eventID == 0)
                return null;

            var query = from gl in _context.GuestLists
                        join c in _context.Customers on gl.CustomerID equals c.CustomerID
                        where gl.EventID == eventID && (c.FirstName.Contains(customerSearch) || c.LastName.Contains(customerSearch)) && !c.Deleted
                        select gl;

            var _guestLists = query.ToList();
            return _guestLists;
        }



        #endregion

    }
}
