﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Exclusive_Software.Model;

namespace Exclusive_Software.Services
{
    /// <summary>
    /// Customer service interface
    /// </summary>
    public partial interface IEventService
    {
        /// <summary>
        /// Gets an Event
        /// </summary>
        /// <param name="EventId">Event identifier</param>
        /// <returns>An event</returns>
        Event GetEventById(int eventId);


        /// <summary>
        /// Marks event as deleted
        /// </summary>
        /// <param name="eventId">Event identifier</param>
        void MarkEventAsDeleted(int eventId);

        /// <summary>
        /// Gets all Assisted events
        /// </summary>
        /// <returns>A event list</returns>
        List<Event> GetAllAssistedEvents(int customerID);

        /// <summary>
        /// Gets all events
        /// </summary>
        /// <returns>A event list</returns>
        List<Event> GetAllEvents();

        /// <summary>
        /// Gets an event by Name
        /// </summary>
        /// <param name="name">Event Name</param>
        /// <returns>A customer</returns>
        List<Event> GetEventByName(string Name);

        /// <summary>
        /// Updates the Event
        /// </summary>
        /// <param name="customer">Event</param>
        void UpdateEvent(Event _event);

        /// <summary>
        /// Inserts an Event
        /// </summary>
        /// <param name="Event">Event</param>
        void InsertEvent(Event _event);

        /// <summary>
        /// Gets an GuestList
        /// </summary>
        /// <param name="guestList">guestList identifier</param>
        /// <returns>An event</returns>
        GuestList GetGuestListById(int guestListID);

        /// <summary>
        /// Gets an GuestList
        /// </summary>
        /// <param name="eventID">event identifier</param>
        /// <returns>An event</returns>
        List<GuestList> GetGuestListByEventId(int eventID);

        /// <summary>
        /// Gets an GuestList
        /// </summary>
        /// <param name="guestList">customer identifier</param>
        /// <returns>list of GuestList</returns>
        List<GuestList> GetGuestListBycustomerId(int customerID);

        /// <summary>
        /// Gets an GuestList
        /// </summary>
        /// <param name="guestList">customer identifier</param>
        /// <returns>list of GuestList</returns>
        bool IsGuestListAsociated(int customerID, int eventID);


        /// <summary>
        /// Gets an GuestList
        /// </summary>
        /// <param name="guestList">customer identifier</param>
        /// <returns>list of GuestList</returns>
        GuestList GetGuestListBycustomerIdAndEventID(int customerID, int eventID);

        /// <summary>
        /// Deletes a GuestList
        /// </summary>
        /// <param name="guestListID">guest List identifier</param>
        void DeleteGuestList(int guestListID);

        /// <summary>
        /// Inserts a GuestList
        /// </summary>
        /// <param name="Event">Event</param>
        void InsertGuestList(GuestList _guestList);

        /// <summary>
        /// Updates the GuestList
        /// </summary>
        /// <param name="_guestList">guestList</param>
        void UpdateGuestList(GuestList _guestList);
    }
}
