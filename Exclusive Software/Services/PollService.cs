﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Exclusive_Software.Model;
using Exclusive_Software.Helpers;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data;
using System.Data.Objects;


namespace Exclusive_Software.Services
{
    public partial class PollService : IPollService
    {
        #region Fields

        /// <summary>
        /// Object context
        /// </summary>
        private readonly ExclusiveDBEntities _context;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="context">Object context</param>
        public PollService()
        {
            this._context = new ExclusiveDBEntities();
        }

        #endregion

        #region Utilities

        /// <summary>
        /// Gets a Poll
        /// </summary>
        /// <param name="pollID">poll identifier</param>
        /// <returns>A poll</returns>
        public Poll GetPollById(int pollID)
        {
            if (pollID == 0)
                return null;

            var query = from p in _context.Polls
                        where p.Poll_ID == pollID && !p.Deleted
                        select p;

            var poll = query.SingleOrDefault();

            return poll;
        }

        /// <summary>
        /// Gets a PollQuestion
        /// </summary>
        /// <param name="pollID">pollQuestion identifier</param>
        /// <returns>A poll Question</returns>
        public PollQuestion GetPollQuestionById(int pollQuestionID)
        {
            if (pollQuestionID == 0)
                return null;

            var query = from pq in _context.PollQuestions
                        where pq.PollQuestionID == pollQuestionID
                        select pq;

            var pollQuestion = query.SingleOrDefault();

            return pollQuestion;
        }

        /// <summary>
        /// Gets a PollAnswer
        /// </summary>
        /// <param name="pollID">PollAnswer identifier</param>
        /// <returns>A poll Answer</returns>
        public PollAnswer GetPollAnswerById(int pollAnswerID)
        {
            if (pollAnswerID == 0)
                return null;

            var query = from pa in _context.PollAnswers
                        where pa.PollAnswerID == pollAnswerID
                        select pa;

            var pollAnswer = query.SingleOrDefault();

            return pollAnswer;
        }

        /// <summary>
        /// Marks poll as deleted
        /// </summary>
        /// <param name="pollId">Poll identifier</param>
        public void MarkPollAsDeleted(int pollId)
        {
            var poll = GetPollById(pollId);
            if (poll != null)
            {
                poll.Deleted = true;
                UpdatePoll(poll);
            }
        }


        /// <summary>
        /// Gets all polls
        /// </summary>
        /// <returns>A poll list</returns>
        public List<Poll> GetAllPolls()
        {

            var query = from p in _context.Polls
                        where !p.Deleted
                        select p;

            var polls = query.ToList();
            return polls;
        }

        /// <summary>
        /// Gets a pollAnswer List by PollQuestionID
        /// </summary>
        /// <param name="PollQuestionID">PollQuestionID</param>
        /// <returns>A poll</returns>
        public List<PollAnswer> GetPollAnswersByPollQuestionID(int pollQuestionID)
        {

            var query = from pa in _context.PollAnswers
                        where pa.PollQuestionID == pollQuestionID
                        orderby pa.DisplayOrder
                        select pa;

            var pollanswers = query.ToList();
            return pollanswers;
        }


        /// <summary>
        /// Gets a PollQuestion List by PollID
        /// </summary>
        /// <param name="PollID">PollID</param>
        /// <returns>A poll Question List</returns>
        public List<PollQuestion> GetPollQuestionByPollID(int PollID)
        {

            var query = from pq in _context.PollQuestions
                        where pq.Poll_ID == PollID
                        orderby pq.DisplayOrder
                        select pq;

            var pollquestions= query.ToList();
            return pollquestions;
        }

        /// <summary>
        /// Gets a poll Answers
        /// </summary>
        /// <param name="Name">Poll Name</param>
        /// <returns>A poll</returns>
        public List<Poll> GetPollByName(string Name)
        {

            var query = from p in _context.Polls
                        where p.PollName.Contains(Name) && !p.Deleted
                        orderby p.PollName
                        select p;
            var polls = query.ToList();
            return polls;
        }

        /// <summary>
        /// Updates the poll
        /// </summary>
        /// <param name="poll">Poll</param>
        public void UpdatePoll(Poll poll)
        {
            if (poll == null)
                throw new ArgumentNullException("UpdatePoll");

            poll.PollName = CommonHelper.EnsureNotNull(poll.PollName);
            poll.PollName = CommonHelper.EnsureMaximumLength(poll.PollName, 255);
            poll.UpdatedOn = DateTime.Now;

            _context.SaveChanges();

        }

        /// <summary>
        /// Updates the poll
        /// </summary>
        /// <param name="pollQuestion">pollQuestion</param>
        public void UpdatePollQuestion(PollQuestion pollQuestion)
        {
            if (pollQuestion == null)
                throw new ArgumentNullException("UpdatePollQuestion");

            pollQuestion.Question = CommonHelper.EnsureNotNull(pollQuestion.Question);
            pollQuestion.UpdatedOn = DateTime.Now;

            _context.SaveChanges();

        }


        /// <summary>
        /// Updates the pollAnswer
        /// </summary>
        /// <param name="pollAnswer">pollAnswer</param>
        public void UpdatePollAnswer(PollAnswer pollAnswer)
        {
            if (pollAnswer == null)
                throw new ArgumentNullException("UpdatePollAnswer");

            pollAnswer.Answer = CommonHelper.EnsureNotNull(pollAnswer.Answer);
            pollAnswer.UpdatedOn = DateTime.Now;

            _context.SaveChanges();

        }

        /// <summary>
        /// Inserts a Poll
        /// </summary>
        /// <param name="poll">Poll</param>
        public void InsertPoll(Poll poll)
        {
            if (poll == null)
                throw new ArgumentNullException("InsertCustomer");

            poll.PollName = CommonHelper.EnsureNotNull(poll.PollName);
            poll.PollName = CommonHelper.EnsureMaximumLength(poll.PollName, 255);
            poll.UpdatedOn = DateTime.Now;
            poll.CreatedOn = DateTime.Now;

            _context.Polls.Add(poll);
            _context.SaveChanges();
        }

        /// <summary>
        /// Inserts a Poll
        /// </summary>
        /// <param name="poll">Poll</param>
        public void InsertPollQuestion(PollQuestion pollQuestion)
        {
            if (pollQuestion == null)
                throw new ArgumentNullException("InsertPollQuestion");

            pollQuestion.Question = CommonHelper.EnsureNotNull(pollQuestion.Question);
            pollQuestion.UpdatedOn = DateTime.Now;
            pollQuestion.CreatedOn = DateTime.Now;

            _context.PollQuestions.Add(pollQuestion);
            _context.SaveChanges();
        }

        /// <summary>
        /// Inserts a Poll Answer
        /// </summary>
        /// <param name="pollAnswer">pollAnswer</param>
        public void InsertPollAnswer(PollAnswer pollAnswer)
        {
            if (pollAnswer == null)
                throw new ArgumentNullException("InsertPollAnswer");

            pollAnswer.Answer = CommonHelper.EnsureNotNull(pollAnswer.Answer);
            pollAnswer.Count = 0;
            pollAnswer.UpdatedOn = DateTime.Now;
            pollAnswer.CreatedOn = DateTime.Now;

            _context.PollAnswers.Add(pollAnswer);
            _context.SaveChanges();
        }

        /// <summary>
        /// Deletes a poll answer
        /// </summary>
        /// <param name="pollAnswerId">Poll answer identifier</param>
        public void DeletePollAnswer(int pollAnswerId)
        {
            var pollAnswer = GetPollAnswerById(pollAnswerId);
            if (pollAnswer == null)
                return;

            _context.PollAnswers.Remove(pollAnswer);
            _context.SaveChanges();

        }

        /// <summary>
        /// Deletes a poll Question
        /// </summary>
        /// <param name="pollAnswerId">Poll Question identifier</param>
        public void DeletePollQuestion(int pollQuestionId)
        {
            var pollQuestion = GetPollQuestionById(pollQuestionId);
            if (pollQuestion == null)
                return;

            _context.PollQuestions.Remove(pollQuestion);
            _context.SaveChanges();

        }

        /// <summary>
        /// Gets a poll Answers
        /// </summary>
        /// <param name="Name">Poll Name</param>
        /// <returns>A poll</returns>
        public Poll GetPollByEventID(int eventID)
        {

            var query = from p in _context.Polls
                        where p.EventID == eventID && !p.Deleted
                        select p;

            var poll = query.SingleOrDefault();

            return poll;
        }

        /// <summary>
        /// Inserts a Poll Answer
        /// </summary>
        /// <param name="pollAnswer">pollAnswer</param>
        public void InsertPollVotingRecord(int pollAnswerID, int customerID)
        {
            if (customerID == 0)
                throw new ArgumentNullException("InsertPollVotingRecord");

            if (pollAnswerID == 0)
                throw new ArgumentNullException("InsertPollVotingRecord");

            PollVotingRecord pvr = new PollVotingRecord();
            pvr.PollAnswerID = pollAnswerID;
            pvr.CustomerId=customerID;
            pvr.CreatedOn = DateTime.Now;
            _context.PollVotingRecords.Add(pvr);
            _context.SaveChanges();

            //Update pollAnswerCount
            PollAnswer AnswertoUpdate = GetPollAnswerById(pollAnswerID);
            AnswertoUpdate.Count = AnswertoUpdate.Count + 1;
            UpdatePollAnswer(AnswertoUpdate);

        }

        /// <summary>
        /// Checks If an anwers was Answered by a customer
        /// </summary>
        /// <param name="PollQuestionID">pollAnswerID</param>
        /// <param name="customerID">customerID</param>
        /// <returns>boll</returns>
        public bool CheckIfAnswered(int pollAnswerID, int customerID)
        {

            var query = from pvr in _context.PollVotingRecords
                        where pvr.PollAnswerID == pollAnswerID && pvr.CustomerId == customerID
                        select pvr;

            var pollanswer = query.SingleOrDefault();

            if (pollanswer != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// Deletes a Poll Voting Record
        /// </summary>
        /// <param name="pollAnswerId">Poll Question identifier</param>
        public void DeletePollVotingRecord(int pollVotingRecordID)
        {
            var pollVotingRecord = GetPollVotingRecord(pollVotingRecordID);
            if (pollVotingRecord == null)
                return;

            _context.PollVotingRecords.Remove(pollVotingRecord);
            _context.SaveChanges();

        }

        /// <summary>
        /// GetPollVotingRecord
        /// </summary>
        /// <param name="PollQuestionID">pollAnswerID</param>
        /// <param name="customerID">customerID</param>
        /// <returns>PollVotingRecord</returns>
        public PollVotingRecord GetPollVotingRecord(int pollVotingRecordID)
        {
            var query = from pvr in _context.PollVotingRecords
                        where pvr.PollVotingRecordID == pollVotingRecordID
                        select pvr;

            var pollVotingRecord = query.SingleOrDefault();

            return pollVotingRecord;
        }

        /// <summary>
        /// Get Poll Voting Record By Answer and Customer
        /// </summary>
        /// <param name="PollQuestionID">pollAnswerID</param>
        /// <param name="customerID">customerID</param>
        /// <returns>boll</returns>
        public PollVotingRecord GetPollVotingRecordByAnswerandCustomer(int pollAnswerID, int customerID)
        {

            var query = from pvr in _context.PollVotingRecords
                        where pvr.PollAnswerID == pollAnswerID && pvr.CustomerId == customerID
                        select pvr;

            var pollVotingRecord = query.SingleOrDefault();

            return pollVotingRecord;
        }


        /// <summary>
        /// Indicate wheter can modify a poll
        /// </summary>
        /// <param name="pollID">pollQuestion identifier</param>
        /// <returns>bool</returns>
        public bool CanModifyPoll(int pollID)
        {
            if (pollID == 0)
                return true;

            var query = from p in _context.Polls
                        join pq in _context.PollQuestions on p.Poll_ID equals pq.Poll_ID
                        join pa in _context.PollAnswers on pq.PollQuestionID equals pa.PollQuestionID
                        join pvr in _context.PollVotingRecords on pa.PollAnswerID equals pvr.PollAnswerID
                        where p.Poll_ID == pollID
                        select pvr;

            var pollVotingRecord = query.ToList();

            if (pollVotingRecord.Count > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        #endregion
    }
}
