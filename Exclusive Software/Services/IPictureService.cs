using System;
using System.Collections.Generic;
using Exclusive_Software.Model;
using Exclusive_Software.Helpers;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Exclusive_Software.Services
{
    /// <summary>
    /// Picture service interface
    /// </summary>
    public partial interface IPictureService
    {

        /// <summary>
        /// Gets a picture
        /// </summary>
        /// <param name="pictureId">Picture identifier</param>
        /// <returns>Picture</returns>
        Picture GetPictureById(int pictureId);

        /// <summary>
        /// Deletes a picture
        /// </summary>
        /// <param name="pictureId">Picture identifier</param>
        void DeletePicture(int pictureId);
        
        /// <summary>
        /// Gets a collection of pictures
        /// </summary>
        /// <param name="pageIndex">Current page</param>
        /// <param name="pageSize">Items on each page</param>
        /// <returns>Paged list of pictures</returns>
        List<Picture> GetAllPictures();
        

        /// <summary>
        /// Inserts a picture
        /// </summary>
        /// <param name="pictureBinary">The picture binary</param>
        /// <param name="mimeType">The picture MIME type</param>
        /// <param name="isNew">A value indicating whether the picture is new</param>
        /// <returns>Picture</returns>
        Picture InsertPicture(byte[] pictureBinary, string mimeType, bool isNew);

        /// <summary>
        /// Updates the picture
        /// </summary>
        /// <param name="pictureId">The picture identifier</param>
        /// <param name="pictureBinary">The picture binary</param>
        /// <param name="mimeType">The picture MIME type</param>
        /// <param name="isNew">A value indicating whether the picture is new</param>
        /// <returns>Picture</returns>
        Picture UpdatePicture(Picture picture);

        /// <summary>
        /// BitmapSourceToByteArray
        /// </summary>
        /// <param name="image">TBitmapSource</param>
        /// <returns>byte[]</returns>
        byte[] BitmapSourceToByteArray(BitmapSource image);

        /// <summary>
        /// ByteToBitmapSource
        /// </summary>
        /// <param name="byte[] image">byte[] image</param>
        /// <returns>BitmapSource</returns>
        BitmapSource ByteToBitmapSource(byte[] image);

    }
}
