﻿using System;
using System.Windows;
using Exclusive_Software.Views;
using Exclusive_Software.Services;
using Exclusive_Software.Model;
using System.Windows.Controls;
using System.Windows.Input;

namespace Exclusive_Software
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Constants

        private Event _assistedEvent;
        private CustomerService customerService = new CustomerService();
        private LogService logService = new LogService();
        private int NavigationStatus;

        #endregion

        #region Ctor
        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>
        public MainWindow()
        {
            try
            {
                InitializeComponent();
                BindData(null);
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.WindowsMain, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        #endregion

        #region Methods

        private void Search()
        {
            if (this.txtSearch.Text.Length >= 3)
            {
                Customers customers = new Customers(ContentFrame);

                if (ContentFrame.Content != customers)
                    ContentFrame.Content = customers;

                customers.FillDataGrid(this.txtSearch.Text);

                this.ButtonCustomerRegister.IsChecked = false;
                this.ButtonEvent.IsChecked = false;
                this.ButtonGuestList.IsChecked = false;
                this.ButtonPoll.IsChecked = false;

                ///Enfoco en el Datagrid
                this.txtSearch.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
            }
            else
            {
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("Debe colocar un criterio de busqueda, solo se permiten busquedas con mas de 3 caracteres en ella.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        #endregion

        #region Events


        public void BindData(Event assistedEvent)
        {
            _assistedEvent = assistedEvent;
            SetecurityStatus();

        }
        /// <summary>
        /// On ButtonHome Click, set homePage to ContentFrame.
        /// </summary>
        /// <param name="sender">The Sender.</param>
        /// <param name="e">The RoutedEventArgs.</param>
        private void ButtonCustomerRegister_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CustomerRegister customersRegister = new CustomerRegister(ContentFrame);

                if (ContentFrame.Content != customersRegister)
                    ContentFrame.Content = customersRegister;

                this.ButtonCustomerRegister.IsChecked = true;
                this.ButtonEvent.IsChecked = false;
                this.ButtonGuestList.IsChecked = false;
                this.ButtonPoll.IsChecked = false;
                this.txtSearch.Text = "";
                NavigationStatus = 0;
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.WindowsMain, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  

        }

        /// <summary>
        /// On ButtonCore Click, set corePage to ContentFrame.
        /// </summary>
        /// <param name="sender">The Sender.</param>
        /// <param name="e">The RoutedEventArgs.</param>
        private void ButtonEvent_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Events events = new Events(ContentFrame);
                if (ContentFrame.Content != events)
                    ContentFrame.Content = events;

                events.FillDataGrid("");

                this.ButtonCustomerRegister.IsChecked = false;
                this.ButtonEvent.IsChecked = true;
                this.ButtonGuestList.IsChecked = false;
                this.ButtonPoll.IsChecked = false;
                this.txtSearch.Text = "";
                NavigationStatus = 1;
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.WindowsMain, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        /// <summary>
        /// On ButtonSDK Click, set sdkPage to ContentFrame.
        /// </summary>
        /// <param name="sender">The Sender.</param>
        /// <param name="e">The RoutedEventArgs.</param>
        private void ButtonGuestList_Click(object sender, RoutedEventArgs e)
        {
            try
            {
            EventAccessControl eventAccessControl = new EventAccessControl(ContentFrame);

            if (ContentFrame.Content != eventAccessControl)
                ContentFrame.Content = eventAccessControl;

            this.ButtonCustomerRegister.IsChecked = false;
            this.ButtonEvent.IsChecked = false;
            this.ButtonGuestList.IsChecked = true;
            this.ButtonPoll.IsChecked = false;
            this.txtSearch.Text = "";
            NavigationStatus = 1;
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.WindowsMain, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        /// <summary>
        /// On ButtonSDK Click, set sdkPage to ContentFrame.
        /// </summary>
        /// <param name="sender">The Sender.</param>
        /// <param name="e">The RoutedEventArgs.</param>
        private void ButtonPoll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
            Polls polls = new Polls(ContentFrame);
            if (ContentFrame.Content != polls)
                ContentFrame.Content = polls;

            polls.FillDataGrid("");

            this.ButtonCustomerRegister.IsChecked = false;
            this.ButtonEvent.IsChecked = false;
            this.ButtonGuestList.IsChecked = false;
            this.ButtonPoll.IsChecked = true;
            this.txtSearch.Text = "";
            NavigationStatus = 2;
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.WindowsMain, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Search();
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.WindowsMain, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SetecurityStatus()
        {
            var ConnectedUser = this.customerService.User;

            if (ConnectedUser != null)
            {
                CustomerRole cr = this.customerService.GetCustomerRoleByCustomerID(ConnectedUser.CustomerID);
                if (cr.CustomerRoleID == 3)
                {
                    this.lblUser.Text = ConnectedUser.FirstName + " " + ConnectedUser.LastName + " - " + cr.Name;
                    this.btnSearch.IsEnabled = false;
                    this.ButtonCustomerRegister.IsEnabled = false;
                    this.ButtonEvent.IsEnabled = false;
                    this.ButtonGuestList.IsEnabled = true;
                    this.ButtonPoll.IsEnabled = false;

                    if (_assistedEvent != null)
                    {
                        CustomerRegister customersRegister = new CustomerRegister(ContentFrame);
                        ContentFrame.Content = customersRegister;
                        customersRegister._assistedEvent = _assistedEvent;
                        customersRegister.BindData(null);
                    }
                    else
                    {
                        ///Abro por defecto la pestana de lista de invitados
                        EventAccessControl eventAccessControl = new EventAccessControl(ContentFrame);
                        if (ContentFrame.Content != eventAccessControl)
                            ContentFrame.Content = eventAccessControl;
                        this.ButtonGuestList.IsChecked = true;
                    }


                }
                else
                {
                    this.lblUser.Text = ConnectedUser.FirstName + " " + ConnectedUser.LastName + " - " + cr.Name;
                    this.btnSearch.IsEnabled = true;
                    this.ButtonCustomerRegister.IsEnabled = true;
                    this.ButtonEvent.IsEnabled = true;
                    this.ButtonGuestList.IsEnabled = true;
                    this.ButtonPoll.IsEnabled = true;
                    ///Abro por defecto la pestana de customer register
                    CustomerRegister customersRegister = new CustomerRegister(ContentFrame);
                    ContentFrame.Content = customersRegister;
                    ButtonCustomerRegister.IsChecked = true;
                }
            }

        }

        private void MainWindowsSearchKey(object sender, KeyEventArgs e)
        {
            if ((e.Key == Key.Enter))
            {
                try
                {
                    this.Search();
                }
                catch (Exception ex)
                {
                    this.logService.InsertLog(LogTypeEnum.WindowsMain, ex.Message, ex);
                    MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        #endregion

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
