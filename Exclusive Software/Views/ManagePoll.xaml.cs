﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Exclusive_Software.Model;
using Exclusive_Software.Services;
using Xceed.Wpf.Toolkit;

namespace Exclusive_Software.Views
{
    /// <summary>
    /// Lógica de interacción para ManagePoll.xaml
    /// </summary>
    public partial class ManagePoll : UserControl
    {
        #region Constants

        ContentControl _contentControl;
        private LogService logService = new LogService();
        private Poll _poll;
        private PollService pollService = new PollService();
        private EventService eventService = new EventService();
        private PictureService pictureService = new PictureService();

        #endregion

        #region Ctor

        public ManagePoll(ContentControl contentControl)
        {
            try
            {
                _contentControl = contentControl;
                InitializeComponent();
                BindData(_poll);
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.ManagePoll, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        #endregion

        public void BindData(Poll poll)
        {
            _poll = poll;

            if (_poll != null)
            {
                this.txtName.Text = _poll.PollName;
                var _events = eventService.GetAllEvents();
                this.cbEventPoll.ItemsSource = _events;

                var _event = eventService.GetEventById(_poll.EventID);

                if(_event !=null)
                {
                    this.cbEventPoll.SelectedItem = _event;
                    this.cbEventPoll.Text = _event.Name;
                    //Avatar
                    if (_event.LogoID != null)
                    {
                        var picture = this.pictureService.GetPictureById((int)_event.LogoID);
                        if (picture != null)
                        {
                            this.imgEventAvatar.Source = this.pictureService.ByteToBitmapSource(picture.PictureBinary);
                        }
                    }
                }
                
                var pollQuestionlist = pollService.GetPollQuestionByPollID(_poll.Poll_ID);

                foreach (PollQuestion pq in pollQuestionlist)
                {
                    PollQuestionView pqView = new PollQuestionView(_contentControl);
                    pqView.BindData(pq, _poll);
                    this.spnlQuestions.Children.Add(pqView);
                }
            }
            else
            {
                CleanControl();
            }
        }

        public void CleanControl()
        {
            _poll = new Poll();
            this.txtName.Text = "";
            var _events = eventService.GetAllEvents();
            this.cbEventPoll.SelectedItem = null;
            this.cbEventPoll.ItemsSource = _events;
            this.lblLocation.Text = "";
            this.lblEventTime.Text = "";
            this.spnlQuestions.Children.Clear();
            //Avatar
            var uriSource = new Uri(@"/Exclusive Software;component/Resources/evento-avatar.png", UriKind.Relative);
            this.imgEventAvatar.Source = new BitmapImage(uriSource);
        }

        private void SavePoll()
        {
            bool IsNew = true;

            if (_poll != null)
            {
                if (_poll.Poll_ID != 0)
                {
                    IsNew = false;
                }
            }

            if (IsNew)
            {
                Poll newPoll = new Poll();

                newPoll.PollName = this.txtName.Text;
                newPoll.Published = true;
                newPoll.Deleted = false;

                var _event = (Event)this.cbEventPoll.SelectedItem;
                if (_event != null)
                {
                    newPoll.EventID = _event.EventID;
                }

                var poll = this.pollService.GetPollByEventID(_event.EventID);
                if (poll == null)
                {
                    pollService.InsertPoll(newPoll);
                    _poll = newPoll;
                }
                else
                {
                    MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("El Evento ya posee la encuesta {0} asociada.",poll.PollName), "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                    MessageBoxResult result2 = System.Windows.MessageBox.Show(string.Format("Desea Actualizar la encuesta {0}?", poll.PollName), "Pregunta", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (result2 == MessageBoxResult.Yes)
                    {
                        ManagePoll managePoll = new ManagePoll(_contentControl);
                        if (_contentControl != null)
                            _contentControl.Content = managePoll;
                        managePoll.BindData(poll);
                    }
                }
            }
            else
            {
                Poll updatePoll = pollService.GetPollById(_poll.Poll_ID);
                
                updatePoll.PollName = this.txtName.Text;
                updatePoll.Published = true;
                updatePoll.Deleted = false;

                var _event = (Event)this.cbEventPoll.SelectedItem;
                if (_event != null)
                {
                    updatePoll.EventID = _event.EventID;
                }

                var poll = this.pollService.GetPollByEventID(_event.EventID);
                if (poll != null)
                {
                    if (updatePoll.Poll_ID == poll.Poll_ID)
                    {
                        pollService.UpdatePoll(updatePoll);
                        _poll = updatePoll;
                    }
                    else
                    {
                        MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("El Evento ya posee la encuesta {0} asociada.", poll.PollName), "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                        MessageBoxResult result2 = System.Windows.MessageBox.Show(string.Format("Desea Actualizar la encuesta {0}?", poll.PollName), "Pregunta", MessageBoxButton.YesNo, MessageBoxImage.Question);
                        if (result2 == MessageBoxResult.Yes)
                        {
                            ManagePoll managePoll = new ManagePoll(_contentControl);
                            if (_contentControl != null)
                                _contentControl.Content = managePoll;
                            managePoll.BindData(poll);
                        }
                    }
                }
                else
                {
                    pollService.UpdatePoll(updatePoll);
                    _poll = updatePoll;
                }
            }

        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.txtName.Text != "")
                {
                    var _event = (Event)this.cbEventPoll.SelectedItem;
                    if (_event != null)
                    {
                        SavePoll();
                        MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("La Encuesta ha Sido Almacenado correctamente", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("Debe asociar la encuesta a un evento.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                else
                {
                    MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("El Nombre de la encuesta es requerida", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.ManagePoll, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        private void btnClean_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CleanControl();
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.ManagePoll, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        private void btnSaveAndClean_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.txtName.Text != "")
                {
                    var _event = (Event)this.cbEventPoll.SelectedItem;
                    if (_event != null)
                    {
                        SavePoll();
                        MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("La Encuesta ha Sido Almacenado correctamente", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                        CleanControl();
                    }
                    else
                    {
                        MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("Debe asociar la encuesta a un evento.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                else
                {
                    MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("El Nombre de la encuesta es requerida", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.ManagePoll, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }


        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_poll != null)
                {
                    if (this.pollService.CanModifyPoll(_poll.Poll_ID))
                    {
                        MessageBoxResult result = System.Windows.MessageBox.Show("Esta usted seguro que desea eliminar esta encuesta y toda su informacion relacionada?", "Pregunta", MessageBoxButton.YesNo, MessageBoxImage.Question);
                        if (result == MessageBoxResult.Yes)
                        {
                            this.pollService.MarkPollAsDeleted(_poll.Poll_ID);
                            MessageBoxResult result2 = Xceed.Wpf.Toolkit.MessageBox.Show("La encuesta se ha borrado correctamente.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                            CleanControl();
                        }
                    }
                    else
                    {
                        MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("La encuesta ya posee respuestas de clientes, no puede ser eliminada.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                else
                {
                    MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("El cliente aun no existe para ser eliminado, si desea borrar todo lo que esta en la pantalla, presione el boton \"Limpiar\"", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.ManagePoll, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        private void btnAddQuestion_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.txtName.Text != "")
                {
                    var _event = (Event)this.cbEventPoll.SelectedItem;
                    if (_event != null)
                    {
                        SavePoll();
                        if (this.pollService.CanModifyPoll(_poll.Poll_ID))
                        {
                            PollQuestionView pq = new PollQuestionView(_contentControl);
                            pq.BindData(null, _poll);
                            this.spnlQuestions.Children.Add(pq);
                        }
                        else
                        {
                            MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("La encuesta ya posee respuestas de clientes, no puede ser modificada.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                    else
                    {
                        MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("Debe asociar la encuesta a un evento.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                else
                {
                    MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("El Nombre de la encuesta es requerida", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.ManagePoll, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        private void cbEventPoll_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var _event = (Event)this.cbEventPoll.SelectedItem;
                if (_event != null)
                {
                    this.lblLocation.Text = _event.Location;
                    this.lblEventTime.Text = String.Format("{0:F}", _event.Time);
                    //Avatar
                    if (_event.LogoID != null)
                    {
                        var picture = this.pictureService.GetPictureById((int)_event.LogoID);
                        if (picture != null)
                        {
                            this.imgEventAvatar.Source = this.pictureService.ByteToBitmapSource(picture.PictureBinary);
                        }
                    }
                    else
                    {
                        //Avatar
                        var uriSource = new Uri(@"/Exclusive Software;component/Resources/evento-avatar.png", UriKind.Relative);
                        this.imgEventAvatar.Source = new BitmapImage(uriSource);
                    }
                }
                else
                {
                    /////Validacion
                }
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.ManagePoll, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }
    }
}
