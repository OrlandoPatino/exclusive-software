﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Exclusive_Software.Services;
using Exclusive_Software.Model;

namespace Exclusive_Software.Views
{
    /// <summary>
    /// Lógica de interacción para Customers.xaml
    /// </summary>
    public partial class Customers : UserControl
    {
        #region Constants

        ContentControl _contentControl;
        private LogService logService = new LogService();
        private CustomerService customerservice = new CustomerService();

        #endregion

        #region Ctor

        public Customers(ContentControl contentControl)
        {
            try
            {
                _contentControl = contentControl;
                InitializeComponent();
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.Customers, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        #endregion

        #region Methods

        public void FillDataGrid(string seachrTerms)
        {
            var customerlist = customerservice.GetCustomerByNameOrCI(seachrTerms);
            this.dgCustomers.ItemsSource = customerlist;

            if (customerlist.Count == 1)
            {
                CustomerRegister customeRegister = new CustomerRegister(_contentControl);
                customeRegister.BindData(customerlist.First());
                if (_contentControl != null)
                    _contentControl.Content = customeRegister;
            }
        }

        #endregion

        #region Events

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_contentControl != null)
                    _contentControl.Content = new CustomerRegister(_contentControl);
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.Customers, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  

        }

        private void dgCustomers_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                CustomerRegister customeRegister = new CustomerRegister(_contentControl);
                IInputElement element = e.MouseDevice.DirectlyOver;
                if (element != null && element is FrameworkElement)
                {
                    var grid = sender as DataGrid;
                    if (grid != null && grid.SelectedItems != null && grid.SelectedItems.Count == 1)
                    {
                        var customer = (Customer)grid.SelectedItem;
                        customeRegister.BindData(customer);
                    }
                }

                if (_contentControl != null)
                    _contentControl.Content = customeRegister;
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.Customers, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        private void dgCustomers_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    CustomerRegister customeRegister = new CustomerRegister(_contentControl);
                    var grid = sender as DataGrid;
                    if (grid != null && grid.SelectedItems != null && grid.SelectedItems.Count == 1)
                    {
                        var customer = (Customer)grid.SelectedItem;
                        customeRegister.BindData(customer);
                    }

                    if (_contentControl != null)
                        _contentControl.Content = customeRegister;
                }
                catch (Exception ex)
                {
                    this.logService.InsertLog(LogTypeEnum.EventAccess, ex.Message, ex);
                    MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        #endregion
    }
}
