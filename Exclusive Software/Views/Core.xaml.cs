﻿using System.Windows.Controls;
using Exclusive_Software.Model;
using System.Collections.Generic;
using System.Linq;
using Exclusive_Software.Services;

namespace Exclusive_Software.Views
{
	/// <summary>
	/// Interaction logic for Core.xaml
	/// </summary>
	public partial class Core : UserControl
	{
        /// <summary>
        /// Initializes a new instance of the Core class.
        /// </summary>
		public Core()
		{
			InitializeComponent();
		}
	}
}
