﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Exclusive_Software.Model;
using Exclusive_Software.Services;
using Xceed.Wpf.Toolkit;
using System.IO;

namespace Exclusive_Software.Views
{
    /// <summary>
    /// Lógica de interacción para EventRegister.xaml
    /// </summary>
    public partial class EventRegister : UserControl
    {
        #region Constants

        ContentControl _contentControl;
        private Event _event;
        private EventService eventService = new EventService();
        private BitmapImage newAvatar = null;
        private PictureService pictureService = new PictureService();
        private PollService pollService = new PollService();
        private LogService logService = new LogService();

        #endregion

        #region Ctor

        public EventRegister(ContentControl contentControl)
        {
            try
            {
                _contentControl = contentControl;
                InitializeComponent();
                BindData(_event);
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.EventRegister, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        #endregion


        public void BindData(Event TheEvent)
        {
            _event = TheEvent;

            if (_event != null)
            {
                this.txtName.Text = _event.Name;
                this.txtLocation.Text = _event.Location;
                this.dtpStartTime.Value = _event.Time;

                //Avatar
                if (TheEvent.LogoID != null)
                {
                    var picture = this.pictureService.GetPictureById((int)TheEvent.LogoID);
                    if (picture != null)
                    {
                        this.imgEventAvatar.Source = this.pictureService.ByteToBitmapSource(picture.PictureBinary);
                    }
                }

                ///Poll Preview
                var _poll = this.pollService.GetPollByEventID(_event.EventID);
                if (_poll != null)
                {
                    var pollQuestions = this.pollService.GetPollQuestionByPollID(_poll.Poll_ID);
                    foreach (PollQuestion pq in pollQuestions)
                    {
                        EventPollQuestion questionView = new EventPollQuestion(_contentControl);
                        questionView.BindData(pq, null);
                        questionView.Name = "questionView_" + pq.PollQuestionID;
                        this.spnlPollPreview.Children.Add(questionView);
                        spnlPollPreview.RegisterName(questionView.Name, questionView);
                    }
                }
            }
            else
            {
                CleanControl();
            }
        }

        public void CleanControl()
        {
            _event = new Event();
            this.txtName.Text = "";
            this.txtLocation.Text = "";
            this.dtpStartTime.Value = null;
            //Avatar
            newAvatar = null;
            var uriSource = new Uri(@"/Exclusive Software;component/Resources/evento-avatar.png", UriKind.Relative);
            this.imgEventAvatar.Source = new BitmapImage(uriSource);
        }

        private void SaveEvent()
        {
            bool IsNew = true;

            if (_event != null)
            {
                if (_event.EventID != 0)
                {
                    IsNew = false;
                }
            }

            if (IsNew)
            {
                Event newEvent = new Event();

                newEvent.Name = this.txtName.Text;
                newEvent.Location = this.txtLocation.Text;
                if (this.dtpStartTime.Value != null)
                {
                    newEvent.Time = (DateTime)this.dtpStartTime.Value;
                }

                //Avatar
                if (newAvatar != null)
                {
                    byte[] array = this.pictureService.BitmapSourceToByteArray(newAvatar);
                    var picture = this.pictureService.InsertPicture(array, "", true);
                    newEvent.LogoID = picture.PictureID;
                }

                eventService.InsertEvent(newEvent);
                _event = newEvent;
            }
            else
            {
                Event updateEvent = this.eventService.GetEventById(_event.EventID);
                updateEvent.Name = this.txtName.Text;
                updateEvent.Location = this.txtLocation.Text;
                if (this.dtpStartTime.Value != null)
                {
                    updateEvent.Time = (DateTime)this.dtpStartTime.Value;
                }
                ///Avatar
                if (newAvatar != null)
                {
                    if (updateEvent.LogoID != null)
                    {
                        var picture = this.pictureService.GetPictureById((int)updateEvent.LogoID);
                        byte[] array = this.pictureService.BitmapSourceToByteArray(newAvatar);
                        picture.PictureBinary = array;
                        picture = this.pictureService.UpdatePicture(picture);
                        updateEvent.LogoID = picture.PictureID;
                    }
                    else
                    {
                        byte[] array = this.pictureService.BitmapSourceToByteArray(newAvatar);
                        var picture = this.pictureService.InsertPicture(array, "", true);
                        updateEvent.LogoID = picture.PictureID;
                    }
                }
                eventService.UpdateEvent(updateEvent);
                _event = updateEvent;
            }

        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.txtName.Text != "")
                {
                    if (this.dtpStartTime.Value != null)
                    {
                        if ((DateTime)this.dtpStartTime.Value >= DateTime.Now)
                        {
                            SaveEvent();
                            MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("El Evento ha Sido Actualizado correctamente", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                        else
                        {
                            MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("La Fecha del evento debe ser a partir de mañana.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                    else
                    {
                        MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("La Fecha del Evento es requerida.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                else
                {
                    MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("El Nombre del Evento es requerido.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.EventRegister, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  

        }

        private void btnClean_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CleanControl();
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.EventRegister, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        private void btnSaveAndClean_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.txtName.Text != "")
                {
                    if (this.dtpStartTime.Value != null)
                    {
                        if ((DateTime)this.dtpStartTime.Value >= DateTime.Now)
                        {
                            SaveEvent();
                            MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("El Evento ha Sido Actualizado correctamente", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                            CleanControl();
                        }
                        else
                        {
                            MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("La Fecha del evento debe ser a partir de mañana.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                    else
                    {
                        MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("La Fecha del Evento es requerida.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                else
                {
                    MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("El Nombre del Evento es requerido.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.EventRegister, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        private void btnLoadImage_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
                dlg.Filter = "Image Files(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|All files (*.*)|*.* ";
                if (dlg.ShowDialog() == true)
                {
                    Stream stream = File.Open(dlg.FileName, FileMode.Open);
                    BitmapImage imgsrc = new BitmapImage();
                    imgsrc.BeginInit();
                    imgsrc.StreamSource = stream;
                    imgsrc.EndInit();
                    this.imgEventAvatar.Source = imgsrc;
                    newAvatar = imgsrc;
                }
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.EventRegister, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  

        }

        private void btnNewPoll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.txtName.Text != "")
                {
                    if (this.dtpStartTime.Value != null)
                    {
                        if ((DateTime)this.dtpStartTime.Value >= DateTime.Now)
                        {
                            SaveEvent();
                            ManagePoll managePoll = new ManagePoll(_contentControl);
                            if (_contentControl != null)
                                _contentControl.Content = managePoll;
                        }
                        else
                        {
                            MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("La Fecha del evento debe ser a partir de mañana.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                    else
                    {
                        MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("La Fecha del Evento es requerida.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                else
                {
                    MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("El Nombre del Evento es requerido.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.EventRegister, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        private void btnUpdatePoll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.txtName.Text != "")
                {
                    if (this.dtpStartTime.Value != null)
                    {
                        if ((DateTime)this.dtpStartTime.Value >= DateTime.Now)
                        {
                            SaveEvent();
                            if (_event != null)
                            {
                                var poll = this.pollService.GetPollByEventID(_event.EventID);
                                if (poll != null)
                                {
                                    ManagePoll managePoll = new ManagePoll(_contentControl);
                                    if (_contentControl != null)
                                        _contentControl.Content = managePoll;
                                    managePoll.BindData(poll);
                                }
                            }
                        }
                        else
                        {
                            MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("La Fecha del evento debe ser a partir de mañana.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                    else
                    {
                        MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("La Fecha del Evento es requerida.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                else
                {
                    MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("El Nombre del Evento es requerido.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.EventRegister, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            } 
        }
    }
}
