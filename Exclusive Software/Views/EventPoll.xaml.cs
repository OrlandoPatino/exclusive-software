﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Exclusive_Software.Model;
using Exclusive_Software.Services;

namespace Exclusive_Software.Views
{
    /// <summary>
    /// Lógica de interacción para EventPoll.xaml
    /// </summary>
    public partial class EventPoll : UserControl
    {
        ContentControl _contentControl;

        private Customer _customer;
        private Poll _poll;
        private Event _event;
        private PollService pollService = new PollService();
        private EventService eventService = new EventService();
        private CustomerService customerService = new CustomerService();
        private PictureService pictureService = new PictureService();
        private LogService logService = new LogService();
        
        /// <summary>
        /// Variable to Navigate throw the system 1 to CustomerRegister 2 EventAccessControl 
        /// </summary>
        private int _NavigationTo;

        public EventPoll(ContentControl contentControl)
        {
            try
            {
                _contentControl = contentControl;
                InitializeComponent();
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.EventPoll, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        public void BindData(Event thisevent, Customer customer, int NavigationTo)
        {
            _NavigationTo = NavigationTo;
            _customer = customer;
            if (_customer != null)
            {
                _event = thisevent;
                if (_event != null)
                {
                    this.lblEventName.Text = _event.Name;
                    this.lblLocation.Text = _event.Location;
                    this.lblEventTime.Text = String.Format("{0:F}", _event.Time);

                    //Avatar
                    if (_event.LogoID != null)
                    {
                        var picture = this.pictureService.GetPictureById((int)_event.LogoID);
                        if (picture != null)
                        {
                            this.imgAvatar.Source = this.pictureService.ByteToBitmapSource(picture.PictureBinary);
                        }
                    }
                    else
                    {
                        //Avatar
                        var uriSource = new Uri(@"/Exclusive Software;component/Resources/evento-avatar.png", UriKind.Relative);
                        this.imgAvatar.Source = new BitmapImage(uriSource);
                    }

                    _poll = this.pollService.GetPollByEventID(_event.EventID);

                    if (_poll != null)
                    {
                        var pollQuestions = this.pollService.GetPollQuestionByPollID(_poll.Poll_ID);
                        foreach (PollQuestion pq in pollQuestions)
                        {
                            EventPollQuestion questionView = new EventPollQuestion(_contentControl);
                            questionView.BindData(pq, customer);
                            questionView.Name = "questionView_" + pq.PollQuestionID;
                            this.spnlQuestions.Children.Add(questionView);
                            spnlQuestions.RegisterName(questionView.Name, questionView);
                        }
                    }
                    else
                    {
                        throw new ArgumentNullException("EventPoll_BindData_poll");
                    }
                }
                else
                {
                    throw new ArgumentNullException("EventPoll_BindData_event");
                }
            }
            else
            {
                throw new ArgumentNullException("EventPoll_BindData_customer");
            }

        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_customer != null)
                {
                    if (_event != null)
                    {
                        if (_poll != null)
                        {
                            var pollQuestions = this.pollService.GetPollQuestionByPollID(_poll.Poll_ID);
                            foreach (PollQuestion pq in pollQuestions)
                            {
                                EventPollQuestion questionView = (EventPollQuestion)this.spnlQuestions.FindName("questionView_" + pq.PollQuestionID);
                                questionView.SaveAnswers();
                            }
                            MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("La encuesta del cliente se ha registrado correctamente", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                            if (_NavigationTo == 2)
                            {
                                EventAccessControl eventAccessControl = new EventAccessControl(_contentControl);
                                if (_contentControl != null)
                                    _contentControl.Content = eventAccessControl;
                                eventAccessControl.BindData(_event);
                            }
                            else
                            {
                                CustomerRegister customerRegister = new CustomerRegister(_contentControl);
                                if (_contentControl != null)
                                    _contentControl.Content = customerRegister;
                                customerRegister.BindData(_customer);
                            }

                        }
                        else
                        {
                            throw new ArgumentNullException("EventPoll_btnSave_Click_poll");
                        }
                    }
                    else
                    {
                        throw new ArgumentNullException("EventPoll_btnSave_Click_event");
                    }
                }
                else
                {
                    throw new ArgumentNullException("EventPoll_btnSave_Click_customer");
                }
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.EventPoll, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_NavigationTo == 2)
                {
                    EventAccessControl eventAccessControl = new EventAccessControl(_contentControl);
                    if (_contentControl != null)
                        _contentControl.Content = eventAccessControl;
                    eventAccessControl.BindData(_event);
                }
                else
                {
                    CustomerRegister customerRegister = new CustomerRegister(_contentControl);
                    if (_contentControl != null)
                        _contentControl.Content = customerRegister;
                    customerRegister.BindData(_customer);
                }
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.EventPoll, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  

        }
    }
}
