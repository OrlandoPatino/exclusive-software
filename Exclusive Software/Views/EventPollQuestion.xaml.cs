﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Exclusive_Software.Model;
using Exclusive_Software.Services;

namespace Exclusive_Software.Views
{
    /// <summary>
    /// Lógica de interacción para EventPollQuestion.xaml
    /// </summary>
    public partial class EventPollQuestion : UserControl
    {
        ContentControl _contentControl;
        private PollQuestion _pollQuestion;
        private Customer _customer;
        private PollService pollService = new PollService();
        private LogService logService = new LogService();


        public EventPollQuestion(ContentControl contentControl)
        {
            try
            {
                _contentControl = contentControl;
                InitializeComponent();
                BindData(_pollQuestion, null);
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.EventPollQuestion, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        public void BindData(PollQuestion pollQuestion, Customer customer)
        {
            _customer = customer;
            _pollQuestion = pollQuestion;
            if (_pollQuestion != null)
            {
                this.lblQuestion.Text = _pollQuestion.Question;

                var Answerlist = pollService.GetPollAnswersByPollQuestionID(_pollQuestion.PollQuestionID);
                foreach (PollAnswer answer in Answerlist)
                {
                    switch (_pollQuestion.AnswerType)
                    {
                        case 1:
                            RadioButton rbAnswer = new RadioButton();
                            // Set properties.
                            rbAnswer.Content = answer.Answer;
                            rbAnswer.Name = "rbAnswer_" + answer.PollAnswerID;
                            rbAnswer.Margin = new Thickness(10, 10, 10, 0);
                            rbAnswer.IsChecked = false;
                            this.ContentStackPanelAnswers.Children.Add(rbAnswer);
                            this.ContentStackPanelAnswers.RegisterName(rbAnswer.Name, rbAnswer);
                            if(customer!=null)
                            {
                                rbAnswer.IsChecked = this.pollService.CheckIfAnswered(answer.PollAnswerID, customer.CustomerID);
                            }
                            break;
                        case 2:
                            CheckBox cbAnswer = new CheckBox();
                            // Set properties.
                            cbAnswer.Content = answer.Answer;
                            cbAnswer.Name = "cbAnswer_" + answer.PollAnswerID;
                            cbAnswer.Margin = new Thickness(10, 10, 10, 0);
                            cbAnswer.IsChecked = false;
                            this.ContentStackPanelAnswers.Children.Add(cbAnswer);
                            this.ContentStackPanelAnswers.RegisterName(cbAnswer.Name, cbAnswer);
                            if (customer != null)
                            {
                                cbAnswer.IsChecked = this.pollService.CheckIfAnswered(answer.PollAnswerID, customer.CustomerID);
                            }
                            break;
                    }
                }
            }
        }

        public List<PollAnswer> SaveAnswers()
        {
            List<PollAnswer> answers = new List<PollAnswer>();

            if (_pollQuestion != null)
            {
                var Answerlist = pollService.GetPollAnswersByPollQuestionID(_pollQuestion.PollQuestionID);
                foreach (PollAnswer answer in Answerlist)
                {
                    //Borro las respuestas anteriorer
                    if (_customer != null)
                    {
                        var pvr = this.pollService.GetPollVotingRecordByAnswerandCustomer(answer.PollAnswerID, _customer.CustomerID);
                        if (pvr != null)
                        {
                            this.pollService.DeletePollVotingRecord(pvr.PollVotingRecordID);
                        }
                    }
                    ///Almaceno las nuevas respuestas
                    switch (_pollQuestion.AnswerType)
                    {
                        case 1:
                            RadioButton rbAnswer = (RadioButton)this.ContentStackPanelAnswers.FindName("rbAnswer_" + answer.PollAnswerID);
                            if (rbAnswer != null)
                            {
                                if ((bool)rbAnswer.IsChecked)
                                {
                                    this.pollService.InsertPollVotingRecord(answer.PollAnswerID, _customer.CustomerID);
                                }
                            }
                            break;
                        case 2:
                            CheckBox cbAnswer = (CheckBox)this.ContentStackPanelAnswers.FindName("cbAnswer_" + answer.PollAnswerID);
                            if (cbAnswer != null)
                            {
                                if ((bool)cbAnswer.IsChecked)
                                {
                                    this.pollService.InsertPollVotingRecord(answer.PollAnswerID, _customer.CustomerID);
                                }
                            }
                            break;
                    }
                }
            }
            else
            {
                throw new ArgumentNullException("EventPollQuestion_SaveAnswers_PollQuestion");
            }
            return answers;
        }

    }
}
