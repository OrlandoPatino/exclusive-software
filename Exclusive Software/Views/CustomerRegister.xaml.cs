﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Exclusive_Software.Model;
using Exclusive_Software.Services;
using Xceed.Wpf.Toolkit;
using Exclusive_Software.Helpers;
using System.IO;
using WebcamControl;
using System.Drawing.Imaging;
using Microsoft.Expression.Encoder.Devices;

namespace Exclusive_Software.Views
{
    /// <summary>
    /// Lógica de interacción para CustomerRegister.xaml
    /// </summary>
    public partial class CustomerRegister : UserControl
    {
        #region Constants

        ContentControl _contentControl;
        public Event _assistedEvent;
        private Customer _customer;
        private CustomerService customerService = new CustomerService();
        private EventService eventService = new EventService();
        private PollService pollService = new PollService();
        private PictureService pictureService = new PictureService();
        private BitmapImage newAvatar = null;
        private LogService logService = new LogService();

        #endregion

        #region Ctor

        public CustomerRegister(ContentControl contentControl)
        {
            try
            {
                _contentControl = contentControl;
                InitializeComponent();
                BindData(_customer);
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.CustomerRegister, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        #endregion

        #region Methods

        public void BindData(Customer customer)
        {
            _customer= customer;
            if(customer!=null)
            {
                this.txtCI.Text = customer.CI;
                this.txtFirstName.Text = customer.FirstName;
                this.txtLastName.Text = customer.LastName;
                this.txtemail.Text = customer.Email;
                this.txtInstagram.Text = customer.Instagram;
                this.txtNotes.Text = customer.Comments;
                this.txtPhone.Text = customer.PhoneNumber;
                this.txtTwitter.Text = customer.Twitter;

                if (customer.Gender == "M")
                {
                    rbGenderMale.IsChecked = true;
                    rbGenderFemale.IsChecked = false;
                }
                else
                {
                    rbGenderFemale.IsChecked = true;
                    rbGenderMale.IsChecked = false;
                }

                this.dpBirthDay.SelectedDate = customer.BirthDay;
                this.cbAdmin.Visibility = Visibility.Visible;
                this.customerRating.Value = Convert.ToDouble(customer.Rating) / 5;
                this.cbAdmin.IsChecked = customer.IsAdmin;
                if(customer.IsAdmin)
                {
                    this.GridPassword.Visibility= Visibility.Visible;
                }

                var customerRoles = customerService.GetCustomerRoles();
                this.cbCustomerRole.ItemsSource = customerRoles;

                if (customer.CustomerRole != null)
                {
                    this.cbCustomerRole.SelectedItem = customer.CustomerRole;
                    this.cbCustomerRole.Text = customer.CustomerRole.Name;
                }
                else
                {
                    //Selecciono el primero
                    if (customerRoles.Count >= 0)
                    {
                        this.cbCustomerRole.SelectedItem = customerRoles.First();
                        this.cbCustomerRole.Text = customerRoles.First().Name;
                    }
                }

                //Avatar
                if(customer.AvatarID!=null)
                {
                    var picture = this.pictureService.GetPictureById((int)customer.AvatarID);
                    if(picture!=null)
                    {
                        this.imgAvatar.Source = this.pictureService.ByteToBitmapSource(picture.PictureBinary);
                    }
                }

                //Loading Pending Events Datagrid
                var pendingEvents = eventService.GetAllPendingEvents();
                List<PendingEvent> items = new List<PendingEvent>();
                foreach (Event e in pendingEvents)
                {
                    items.Add(new PendingEvent() { ID = e.EventID, Name = e.Name, Attend = this.customerService.CustomerAttendToEvent(customer.CustomerID,e.EventID) });
                }
                this.dgEvents.ItemsSource = items;

                //Loading Assited Events Datagrid
                var assistedEvents = eventService.GetAllAssistedEvents(customer.CustomerID);
                this.dgAssistedEvents.ItemsSource = assistedEvents;

            }
            else
            {
                CleanControl();
            }

            SetSecurityStatus();

        }

        public void CleanControl()
        {
            _customer = new Customer();
            this.txtCI.Text = "";
            this.txtFirstName.Text = "";
            this.txtLastName.Text = "";
            this.txtemail.Text = "";
            this.txtInstagram.Text = "";
            this.txtNotes.Text = "";
            this.txtPhone.Text = "";
            this.txtTwitter.Text = "";
            this.rbGenderMale.IsChecked = true;
            this.dpBirthDay.SelectedDate = Convert.ToDateTime("01/01/1990");
            this.cbAdmin.Visibility = Visibility.Hidden;
            this.GridPassword.Visibility = Visibility.Hidden;
            this.customerRating.Value = 0;

            var customerRoles = customerService.GetCustomerRoles();
            this.cbCustomerRole.ItemsSource = customerRoles;
            //Selecciono el primero
            if (customerRoles.Count >= 0)
            {
                this.cbCustomerRole.SelectedItem = customerRoles.First();
                this.cbCustomerRole.Text = customerRoles.First().Name;
            }

            //Avatar
            newAvatar = null;
            var uriSource = new Uri(@"/Exclusive Software;component/Resources/avatar.png", UriKind.Relative);
            this.imgAvatar.Source = new BitmapImage(uriSource);

            //Loading Pending Events Datagrid
            var pendingEvents = eventService.GetAllPendingEvents();
            List<PendingEvent> items = new List<PendingEvent>();
            foreach (Event e in pendingEvents)
            {
                bool attend = false;
                if (_assistedEvent != null)
                {
                    if (_assistedEvent.EventID == e.EventID)
                    {
                        attend = true;
                    }
                }
                items.Add(new PendingEvent() { ID = e.EventID, Name = e.Name, Attend = attend });
            }
            this.dgEvents.ItemsSource = items;

            List<Event> assistedEvents = new List<Event>();
            this.dgAssistedEvents.ItemsSource=assistedEvents;
        }

        private void SaveCustumer()
        {
            bool IsNew = true;

            if(_customer!= null)
            {
                if (_customer.CustomerID != 0)
                {
                    IsNew = false;
                }
            }

            if (IsNew)
            {
                Customer customer = new Customer();

                customer.CI = this.txtCI.Text;
                customer.FirstName = this.txtFirstName.Text;
                customer.LastName = this.txtLastName.Text;
                customer.Email=this.txtemail.Text ;
                customer.Instagram =this.txtInstagram.Text;
                customer.Comments=this.txtNotes.Text;
                customer.PhoneNumber= this.txtPhone.Text;
                customer.Twitter =this.txtTwitter.Text;

                if(rbGenderMale.IsChecked != null)
                {
                    if ((bool)rbGenderMale.IsChecked)
                    {
                        customer.Gender = "M";
                    }
                }

                if (rbGenderFemale.IsChecked != null)
                {
                    if ((bool)rbGenderFemale.IsChecked)
                    {
                        customer.Gender = "F";
                    }
                }

                if (this.dpBirthDay.SelectedDate != null)
                {
                    customer.BirthDay = (DateTime)this.dpBirthDay.SelectedDate;
                }

                customer.IsAdmin = false;
                customer.RegistrationDate = DateTime.Now;
                customer.Deleted = false;
                customer.Rating = Convert.ToInt32(this.customerRating.Value);

                //Avatar
                if (newAvatar != null)
                {
                    byte[] array = this.pictureService.BitmapSourceToByteArray(newAvatar);
                    var picture = this.pictureService.InsertPicture(array, "", true);
                    customer.AvatarID = picture.PictureID;
                }

                customerService.InsertCustomer(customer);

                //Insert Customer Events
                if (this.dgEvents.Visibility == Visibility.Visible)
                {
                    for (int i = 0; i < dgEvents.Items.Count; i++)
                    {
                        var Row = dgEvents.GetRow(i);
                        var columnCell = dgEvents.GetCell(Row, 0);
                        var coleventId = Convert.ToInt32(((TextBlock)columnCell.Content).Text);
                        columnCell = dgEvents.GetCell(Row, 2);
                        CheckBox ck = DataGridHelper.GetVisualChild<CheckBox>(columnCell);
                        if ((bool)ck.IsChecked)
                        {
                            GuestList newguestlist = new GuestList();
                            newguestlist.EventID = coleventId;
                            newguestlist.CustomerID = customer.CustomerID;
                            newguestlist.Attended = false;
                            this.eventService.InsertGuestList(newguestlist);
                        }
                    }
                }

                if (_assistedEvent != null)
                {
                    if (!this.eventService.IsGuestListAsociated(customer.CustomerID, _assistedEvent.EventID))
                    {
                        GuestList newguestlist = new GuestList();
                        newguestlist.EventID = _assistedEvent.EventID;
                        newguestlist.CustomerID = customer.CustomerID;
                        newguestlist.Attended = true;
                        newguestlist.AttendedDate = DateTime.Now;
                        this.eventService.InsertGuestList(newguestlist);
                    }
                    else
                    {
                        GuestList updateguestlist = this.eventService.GetGuestListBycustomerIdAndEventID(customer.CustomerID, _assistedEvent.EventID);
                        updateguestlist.Attended = true;
                        updateguestlist.AttendedDate = DateTime.Now;
                         this.eventService.UpdateGuestList(updateguestlist);
                    }

                    if (this.pollService.GetPollByEventID(_assistedEvent.EventID) != null)
                    {
                        ///Debe llenar la encuesta
                        EventPoll eventPoll = new EventPoll(_contentControl);
                        eventPoll.BindData(_assistedEvent, customer, 2);

                        if (_contentControl != null)
                            _contentControl.Content = eventPoll;
                    }
                    else
                    {
                        ///Lo llevo de nuevo al evento
                        EventAccessControl eventAccessControl = new EventAccessControl(_contentControl);
                        eventAccessControl.BindData(_assistedEvent);
                        if (_contentControl != null)
                            _contentControl.Content = eventAccessControl;
                    }
                }

                _customer = customer;
            }
            else
            {
                Customer customer = this.customerService.GetCustomerById(_customer.CustomerID);
                customer.CI = this.txtCI.Text;
                customer.FirstName = this.txtFirstName.Text;
                customer.LastName = this.txtLastName.Text;
                customer.Email = this.txtemail.Text;
                customer.Instagram = this.txtInstagram.Text;
                customer.Comments = this.txtNotes.Text;
                customer.PhoneNumber = this.txtPhone.Text;
                customer.Twitter = this.txtTwitter.Text;

                if (rbGenderMale.IsChecked != null)
                {
                    if ((bool)rbGenderMale.IsChecked)
                    {
                        customer.Gender = "M";
                    }
                }

                if (rbGenderFemale.IsChecked != null)
                {
                    if ((bool)rbGenderFemale.IsChecked)
                    {
                        customer.Gender = "F";
                    }
                }

                if (this.dpBirthDay.SelectedDate != null)
                {
                    customer.BirthDay = (DateTime)this.dpBirthDay.SelectedDate;
                }

                customer.Rating = Convert.ToInt32(this.customerRating.Value*5);

                if (this.cbAdmin.IsChecked != null)
                {
                    customer.IsAdmin = (bool)this.cbAdmin.IsChecked;
                    if ((bool)this.cbAdmin.IsChecked)
                    {
                        if (this.cbCustomerRole.SelectedItem != null)
                        {
                            CustomerRole cr = (CustomerRole)this.cbCustomerRole.SelectedItem;
                            customer.CustomerRoleID = cr.CustomerRoleID;
                        }
                    }
                }
                else
                {
                    customer.IsAdmin = false;
                }

                ///Avatar
                if (newAvatar != null)
                {
                    if (customer.AvatarID != null)
                    {
                        var picture = this.pictureService.GetPictureById((int)customer.AvatarID);
                        if (picture != null)
                        {
                            byte[] array = this.pictureService.BitmapSourceToByteArray(newAvatar);
                            picture.PictureBinary = array;
                            picture = this.pictureService.UpdatePicture(picture);
                            customer.AvatarID = picture.PictureID;
                        }
                        else
                        {
                            byte[] array = this.pictureService.BitmapSourceToByteArray(newAvatar);
                            picture = this.pictureService.InsertPicture(array, "", true);
                            customer.AvatarID = picture.PictureID;
                        }
                    }
                    else
                    {
                        byte[] array = this.pictureService.BitmapSourceToByteArray(newAvatar);
                        var picture = this.pictureService.InsertPicture(array, "", true);
                        customer.AvatarID = picture.PictureID;
                    }
                }

                customerService.UpdateCustomer(customer);

                //Insert Customer Events
                if (this.dgEvents.Visibility == Visibility.Visible)
                {
                    for (int i = 0; i < dgEvents.Items.Count; i++)
                    {
                        var Row = dgEvents.GetRow(i);
                        var columnCell = dgEvents.GetCell(Row, 0);
                        var coleventId = Convert.ToInt32(((TextBlock)columnCell.Content).Text);

                        columnCell = dgEvents.GetCell(Row, 2);
                        CheckBox ck = DataGridHelper.GetVisualChild<CheckBox>(columnCell);
                        if ((bool)ck.IsChecked)
                        {
                            if (!this.eventService.IsGuestListAsociated(customer.CustomerID, coleventId))
                            {
                                GuestList newguestlist = new GuestList();
                                newguestlist.EventID = coleventId;
                                newguestlist.CustomerID = customer.CustomerID;
                                newguestlist.Attended = false;
                                this.eventService.InsertGuestList(newguestlist);
                            }
                        }
                        else
                        {
                            if (this.eventService.IsGuestListAsociated(customer.CustomerID, coleventId))
                            {
                                GuestList oldguestlist = this.eventService.GetGuestListBycustomerIdAndEventID(customer.CustomerID, coleventId);
                                if (!oldguestlist.Attended)
                                {
                                    this.eventService.DeleteGuestList(oldguestlist.GuestListID);
                                }
                            }
                        }
                    }
                }


                if (_assistedEvent != null)
                {
                    if (!this.eventService.IsGuestListAsociated(customer.CustomerID, _assistedEvent.EventID))
                    {
                        GuestList newguestlist = new GuestList();
                        newguestlist.EventID = _assistedEvent.EventID;
                        newguestlist.CustomerID = customer.CustomerID;
                        newguestlist.Attended = true;
                        newguestlist.AttendedDate = DateTime.Now;
                        this.eventService.InsertGuestList(newguestlist);
                    }
                    else
                    {
                        GuestList updateguestlist = this.eventService.GetGuestListBycustomerIdAndEventID(customer.CustomerID, _assistedEvent.EventID);
                        updateguestlist.Attended = true;
                        updateguestlist.AttendedDate = DateTime.Now;
                        this.eventService.UpdateGuestList(updateguestlist);
                    }

                    if (this.pollService.GetPollByEventID(_assistedEvent.EventID) != null)
                    {
                        ///Debe llenar la encuesta
                        EventPoll eventPoll = new EventPoll(_contentControl);
                        eventPoll.BindData(_assistedEvent, customer,2);

                        if (_contentControl != null)
                            _contentControl.Content = eventPoll;
                    }
                    else
                    {
                        ///Lo llevo de nuevo al evento
                        EventAccessControl eventAccessControl = new EventAccessControl(_contentControl);
                        eventAccessControl.BindData(_assistedEvent);
                        if (_contentControl != null)
                            _contentControl.Content = eventAccessControl;
                    }
                }
            }
        }

        private void SetSecurityStatus()
        {
            var ConnectedUser = this.customerService.User;
            if (ConnectedUser != null)
            {
                CustomerRole cr = this.customerService.GetCustomerRoleByCustomerID(ConnectedUser.CustomerID);
                if (cr.CustomerRoleID == 3)//usuario basico
                {
                    this.dgEvents.Visibility = Visibility.Collapsed;
                    this.cbAdmin.Visibility = Visibility.Collapsed;
                    this.GridPassword.Visibility = Visibility.Hidden;
                    this.lblPendingEvents.Visibility = Visibility.Hidden;
                    this.btnClean.Visibility = Visibility.Hidden;
                    this.btnSaveAndClean.Visibility = Visibility.Hidden;
                    this.btnDelete.Visibility = Visibility.Hidden;

                }
                else
                {
                    this.dgEvents.Visibility = Visibility.Visible;
                    this.cbAdmin.Visibility = Visibility.Visible;
                    this.lblPendingEvents.Visibility = Visibility.Visible;
                    this.btnClean.Visibility = Visibility.Visible;
                    this.btnSaveAndClean.Visibility = Visibility.Visible;
                    this.btnDelete.Visibility = Visibility.Visible;
                }
            }
        }

        #endregion

        #region Events

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((bool)this.cbAdmin.IsChecked)
                {
                    this.GridPassword.Visibility = Visibility.Visible;
                }
                else
                {
                    this.GridPassword.Visibility = Visibility.Hidden;
                }
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.CustomerRegister, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        private void btnStorePassword_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_customer != null)
                {
                    if (this.txtPassword.Password == this.txtReenterPassword.Password && (bool)cbAdmin.IsChecked)
                    {
                        if (_customer.CustomerID != 0)
                        {
                            Customer customer = this.customerService.GetCustomerById(_customer.CustomerID);
                            customer.IsAdmin = true;
                            customer.PasswordHash = this.txtPassword.Password;
                            var customerRole = (CustomerRole)this.cbCustomerRole.SelectedItem;
                            if (customerRole != null)
                            {
                                customer.CustomerRoleID = customerRole.CustomerRoleID;
                                customerService.UpdateCustomer(customer);
                                _customer = customer;
                                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("La contraseña de acceso al sistema se ha almacenado correctamente", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                            else
                            {
                                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("Debe seleccionar el rol del cliente.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                        }
                        else
                        {
                            MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("El cliente no existe aun, debe guardarlo y luego crear su perfil de seguridad", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                    else
                    {
                        MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("Las contraseñas no coinciden", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                else
                {
                    MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("El cliente no existe aun, debe guardarlo y luego crear su perfil de seguridad", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.CustomerRegister, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.txtCI.Text != "")
                {
                    if (this.txtFirstName.Text != "" || this.txtLastName.Text != "")
                    {
                        if (CommonHelper.IsValidEmail(this.txtemail.Text) || this.txtemail.Text=="")
                        {
                            if (this.txtCI.Text != "")
                            {
                                Customer c =this.customerService.GetCustomerbyCI(this.txtCI.Text);
                                if (c != null && _customer.CustomerID == 0)
                                {
                                    MessageBoxResult result = System.Windows.MessageBox.Show("Ya existe un cliente Registrado con la misma Cedula de Identidad, desea actualizar su informacion?", "Pregunta", MessageBoxButton.YesNo, MessageBoxImage.Question);
                                    if (result == MessageBoxResult.Yes)
                                    {
                                        CustomerRegister customeRegister = new CustomerRegister(_contentControl);
                                        customeRegister.BindData(c);
                                        customeRegister._assistedEvent = _assistedEvent;
                                        if (_contentControl != null)
                                            _contentControl.Content = customeRegister;
                                    }
                                }
                                else
                                {
                                    SaveCustumer();
                                    MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("El Cliente ha Sido Actualizado correctamente", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                                }
                            }
                            else
                            {
                                SaveCustumer();
                                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("El Cliente ha Sido Actualizado correctamente", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                        }
                        else
                        {
                            MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("El correo no cumple con el formato adecuado.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                    else
                    {
                        MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("El nombre del cliente y el apellido son requeridos.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                else
                {
                    MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("La cédula del cliente es requerida.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.CustomerRegister, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        private void btnClean_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CleanControl();
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.CustomerRegister, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  

        }

        private void btnSaveAndClean_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.txtCI.Text != "")
                {
                    if (this.txtFirstName.Text != "" || this.txtLastName.Text != "")
                    {
                        if (CommonHelper.IsValidEmail(this.txtemail.Text) || this.txtemail.Text == "")
                        {
                            if (this.txtCI.Text != "")
                            {
                                Customer c = this.customerService.GetCustomerbyCI(this.txtCI.Text);
                                if (c != null && _customer.CustomerID == 0)
                                {
                                    MessageBoxResult result = System.Windows.MessageBox.Show("Ya existe un cliente Registrado con la misma Cedula de Identidad, desea actualizar su informacion?", "Pregunta", MessageBoxButton.YesNo, MessageBoxImage.Question);
                                    if (result == MessageBoxResult.Yes)
                                    {
                                        CustomerRegister customeRegister = new CustomerRegister(_contentControl);
                                        customeRegister.BindData(c);
                                        customeRegister._assistedEvent = _assistedEvent;
                                        if (_contentControl != null)
                                            _contentControl.Content = customeRegister;
                                    }
                                }
                                else
                                {
                                    SaveCustumer();
                                    MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("El Cliente ha Sido Actualizado correctamente", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                                    CleanControl();
                                }
                            }
                            else
                            {
                                SaveCustumer();
                                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("El Cliente ha Sido Actualizado correctamente", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                                CleanControl();
                            }
                        }
                        else
                        {
                            MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("El correo no cumple con el formato adecuado.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                    else
                    {
                        MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("El nombre del cliente y el apellido son requeridos.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                else
                {
                    MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("La cédula del cliente es requerida.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.CustomerRegister, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        private void btnStartCapture_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string imagePath = @"C:\WebcamSnapshots";

                if (!Directory.Exists(imagePath))
                {
                    Directory.CreateDirectory(imagePath);
                }

                WebcamCtrl.ImageDirectory = imagePath;
                // Find available a/v devices
                WebcamCtrl.FrameSize = new System.Drawing.Size(640, 480);
                var vidDevices = EncoderDevices.FindDevices(EncoderDeviceType.Video);
                WebcamCtrl.VideoDevice = vidDevices.FirstOrDefault();
                ///////
                // Display webcam video
                WebcamCtrl.StartCapture();
            }
            catch (Microsoft.Expression.Encoder.SystemErrorException ex)
            {
                Xceed.Wpf.Toolkit.MessageBox.Show("Device is in use by another application");
            }   
        }

        private void btnStopCapture_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Stop the display of webcam video.
                WebcamCtrl.StopCapture();
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.CustomerRegister, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        private void btnTakeSnapshot_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Take snapshot of webcam video.
                WebcamCtrl.TakeSnapshot();
                // Stop the display of webcam video.
                WebcamCtrl.StopCapture();
                Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
                dlg.InitialDirectory = @"C:\WebcamSnapshots";
                dlg.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.JPEG)|*.BMP;*.JPG;*.GIF;*.JPEG|All files (*.*)|*.* ";
                if (dlg.ShowDialog() == true)
                {
                    Stream stream = File.Open(dlg.FileName, FileMode.Open);
                    BitmapImage imgsrc = new BitmapImage();
                    imgsrc.BeginInit();
                    imgsrc.StreamSource = stream;
                    imgsrc.EndInit();
                    imgAvatar.Source = imgsrc;
                    newAvatar = imgsrc;
                }
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.CustomerRegister, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  

        }

        private void btnLoadImage_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
                dlg.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.JPEG)|*.BMP;*.JPG;*.GIF;*.JPEG|All files (*.*)|*.* ";
                if (dlg.ShowDialog() == true)
                {
                    Stream stream = File.Open(dlg.FileName, FileMode.Open);
                    BitmapImage imgsrc = new BitmapImage();
                    imgsrc.BeginInit();
                    imgsrc.StreamSource = stream;
                    imgsrc.EndInit();
                    imgAvatar.Source = imgsrc;
                    newAvatar = imgsrc;
                }
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.CustomerRegister, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        void btnShowPoll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                for (var vis = sender as Visual; vis != null; vis = VisualTreeHelper.GetParent(vis) as Visual)
                    if (vis is DataGridRow)
                    {
                        var row = (DataGridRow)vis;
                        var columnCell = this.dgAssistedEvents.GetCell(row, 0);
                        var coleventId = Convert.ToInt32(((TextBlock)columnCell.Content).Text);

                        var poll = pollService.GetPollByEventID(coleventId);
                        var theEvent = eventService.GetEventById(coleventId);
                        if (poll != null)
                        {
                            ///Debe llenar la encuesta
                            EventPoll eventPoll = new EventPoll(_contentControl);
                            eventPoll.BindData(theEvent, _customer,1); ///Aqui debo cargarle las respuestas que dio el cliente.

                            if (_contentControl != null)
                                _contentControl.Content = eventPoll;
                        }
                        else
                        {
                            MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("Para este evento no se generó encuesta.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                        break;
                    }
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.CustomerRegister, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        private void txtCI_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!char.IsDigit(e.Text, e.Text.Length - 1))
            {
                e.Handled = true;

            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_customer != null)
                {
                    MessageBoxResult result = System.Windows.MessageBox.Show("Esta usted seguro que desea eliminar este cliente y toda su informacion relacionada?", "Pregunta", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (result == MessageBoxResult.Yes)
                    {
                        this.customerService.MarkCustomerAsDeleted(_customer.CustomerID);
                        MessageBoxResult result2 = Xceed.Wpf.Toolkit.MessageBox.Show("El cliente se ha borrado correctamente.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                        CleanControl();
                    }
                }
                else
                {
                    MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("El cliente aun no existe para ser eliminado, si desea borrar todo lo que esta en la pantalla, presione el boton \"Limpiar\"", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.CustomerRegister, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        #endregion

        #region Helpers Class

        public class PendingEvent
        {
            public int ID { get; set; }
            public string Name { get; set; }
            public bool Attend { get; set; }
        }

        #endregion
    }
}
