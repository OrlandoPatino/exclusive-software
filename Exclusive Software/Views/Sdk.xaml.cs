﻿using System.Windows.Controls;

namespace Exclusive_Software.Views
{
    /// <summary>
    /// Interaction logic for Sdk.xaml
    /// </summary>
    public partial class Sdk : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the Sdk class.
        /// </summary>
        public Sdk()
        {
            InitializeComponent();
        }
    }
}