﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Exclusive_Software.Services;
using Exclusive_Software.Model;

namespace Exclusive_Software.Views
{
    /// <summary>
    /// Lógica de interacción para Polls.xaml
    /// </summary>
    public partial class Polls : UserControl
    {
        
        ContentControl _contentControl;
        private PollService pollService = new PollService();
        private LogService logService = new LogService();

        public Polls(ContentControl contentControl)
        {
            try
            {
                _contentControl = contentControl;
                InitializeComponent();
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.Polls, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        public void FillDataGrid(string seachrTerms)
        {
            try
            {
                var polllist = pollService.GetPollByName(seachrTerms);
                this.dgPolls.ItemsSource = polllist;
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.Polls, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_contentControl != null)
                    _contentControl.Content = new ManagePoll(_contentControl);
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.Polls, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        private void dgPolls_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                ManagePoll managePoll = new ManagePoll(_contentControl);
                IInputElement element = e.MouseDevice.DirectlyOver;
                if (element != null && element is FrameworkElement)
                {
                    var grid = sender as DataGrid;
                    if (grid != null && grid.SelectedItems != null && grid.SelectedItems.Count == 1)
                    {
                        var _poll = (Poll)grid.SelectedItem;
                        managePoll.BindData(_poll);
                    }
                }

                if (_contentControl != null)
                    _contentControl.Content = managePoll;
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.Polls, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }
    }
}
