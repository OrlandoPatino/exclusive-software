﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Exclusive_Software.Model;
using Exclusive_Software.Services;
using Exclusive_Software.Helpers;

namespace Exclusive_Software.Views
{
    /// <summary>
    /// Lógica de interacción para EventAccessControl.xaml
    /// </summary>
    public partial class EventAccessControl : UserControl
    {
        #region Constants

        ContentControl _contentControl;
        private Event _event;
        private PollService pollService = new PollService();
        private EventService eventService = new EventService();
        private CustomerService customerService = new CustomerService();
        private PictureService pictureService = new PictureService();
        private LogService logService = new LogService();

        #endregion

        #region Ctor

        public EventAccessControl(ContentControl contentControl)
        {
            try
            {
                _contentControl = contentControl;
                InitializeComponent();
                BindData(_event);
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.EventAccess, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        #endregion

        #region Methods

        public void BindData(Event _eventSelected)
        {
            _event = _eventSelected;
            var _events = eventService.GetAllPendingEvents();
            this.cbEvent.ItemsSource = _events;

            if (_event != null)
            {
                this.cbEvent.SelectedItem = _event;
                this.cbEvent.Text = _event.Name;
            }
            else
            {
                //Selecciono el primero
                if(_events.Count>0)
                {
                    _event = _events.First();
                    this.cbEvent.SelectedItem = _event;
                    this.cbEvent.Text = _event.Name;
                }
            }
        }

        private void Search()
        {
            if (this.txtSearch.Text.Length >= 3)
            {
                var _event = (Event)this.cbEvent.SelectedItem;
                if (_event != null)
                {
                    List<CustomerListForGuestList> customerList = new List<CustomerListForGuestList>();
                    var customers = customerService.GetCustomerByNameOrCI(this.txtSearch.Text);
                    foreach (Customer customerItem in customers)
                    {
                        CustomerListForGuestList clItem = new CustomerListForGuestList();
                        clItem.CustomerID = customerItem.CustomerID;
                        clItem.CI = customerItem.CI;
                        clItem.FirstName = customerItem.FirstName;
                        clItem.LastName = customerItem.LastName;
                        var guestlist = eventService.GetGuestListBycustomerIdAndEventID(customerItem.CustomerID, _event.EventID);
                        if (guestlist != null)
                        {
                            clItem.Attended = guestlist.Attended;
                            clItem.Invited = true;
                        }
                        else
                        {
                            clItem.Attended = false;
                            clItem.Invited = false;
                        }
                        customerList.Add(clItem);
                    }
                    this.dgCustomers.ItemsSource = customerList;

                    ///Si encuentra 1 registro hace entrada de una vez
                    if (customerList.Count == 1)
                    {
                        GetAccessToCustomer(_event, customerList.First());
                    }
                }
                else
                {
                    MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("Dede Seleccionar algun evento.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            else
            {
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("Debe colocar un criterio de busqueda, solo se permiten busquedas con mas de 3 caracteres en ella.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void GetAccessToCustomer(Event _event, CustomerListForGuestList CustomerListItem)
        {
            if (_event != null)
            {
                if (CustomerListItem != null)
                {
                    if (CustomerListItem.Invited)
                    {
                        MessageBoxResult result = MessageBox.Show(string.Format("Esta usted seguro que desea registrar la entrada del cliente {0} {1} al evento?", CustomerListItem.FirstName, CustomerListItem.LastName), "Pregunta", MessageBoxButton.YesNo, MessageBoxImage.Question);
                        if (result == MessageBoxResult.Yes)
                        {
                            var guestList = eventService.GetGuestListBycustomerIdAndEventID(CustomerListItem.CustomerID, _event.EventID);
                            guestList.Attended = true;
                            guestList.AttendedDate = DateTime.Now;
                            this.eventService.UpdateGuestList(guestList);
                            MessageBoxResult result2 = Xceed.Wpf.Toolkit.MessageBox.Show("El acceso al cliente se ha registrado correctamente", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                            //Limpio el DG y el buscador
                            this.dgCustomers.ItemsSource = new List<GuestList>();
                            this.txtSearch.Text = "";
                            ///Actualizo los contadores
                            var guestlist = eventService.GetGuestListByEventId(_event.EventID);
                            this.lblGuestCount.Text = guestlist.Count.ToString();
                            this.lblGuestIn.Text = guestlist.Where(x => x.Attended).ToList().Count.ToString();
                            //Funcion de Actualizacion de cliente luego del ingreso desabilitada
                            //CustomerRegister customeRegister = new CustomerRegister(_contentControl);
                            //Customer c = this.customerService.GetCustomerById(guestList.CustomerID);
                            //if (c != null)
                            //{
                            //    customeRegister.BindData(c);
                            //    customeRegister._assistedEvent = guestList.Event;
                            //    if (_contentControl != null)
                            //        _contentControl.Content = customeRegister;
                            //}
                        }
                    }
                    else
                    {
                        var ConnectedUser = this.customerService.User;
                        if (ConnectedUser != null)
                        {
                            CustomerRole cr = this.customerService.GetCustomerRoleByCustomerID(ConnectedUser.CustomerID);
                            if (cr.CustomerRoleID == 3)/// Usuario Basico.
                            {
                                var customerItem = this.customerService.GetCustomerById(CustomerListItem.CustomerID);
                                //Limpio el DG y el buscador
                                this.dgCustomers.ItemsSource = new List<GuestList>();
                                this.txtSearch.Text = "";
                                ///
                                WindowSpecialAccess wsa = new WindowSpecialAccess(_event, customerItem);
                                wsa.Show();
                                wsa.Topmost = true;
                                wsa.Focus();

                            }
                            else
                            {
                                MessageBoxResult result = MessageBox.Show(string.Format("Esta usted seguro que desea Invitar al cliente {0} {1} y registrar su entrada al evento?", CustomerListItem.FirstName, CustomerListItem.LastName), "Pregunta", MessageBoxButton.YesNo, MessageBoxImage.Question);
                                if (result == MessageBoxResult.Yes)
                                {
                                    var guestList = new GuestList();
                                    guestList.CustomerID = CustomerListItem.CustomerID;
                                    guestList.EventID = _event.EventID;
                                    guestList.Attended = true;
                                    guestList.AttendedDate = DateTime.Now;
                                    this.eventService.InsertGuestList(guestList);
                                    MessageBoxResult result2 = Xceed.Wpf.Toolkit.MessageBox.Show("El acceso al cliente se ha registrado correctamente", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                                    //Limpio el DG y el buscador
                                    this.dgCustomers.ItemsSource = new List<GuestList>();
                                    this.txtSearch.Text = "";
                                    ///Actualizo los contadores
                                    var guestlist = eventService.GetGuestListByEventId(_event.EventID);
                                    this.lblGuestCount.Text = guestlist.Count.ToString();
                                    this.lblGuestIn.Text = guestlist.Where(x => x.Attended).ToList().Count.ToString();
                                }
                            }
                        }
                    }
                }
            }
        }
        
        #endregion

        #region Events


        private void cbEvent_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var _event = (Event)this.cbEvent.SelectedItem;
                if (_event != null)
                {
                    this.lblLocation.Text = _event.Name;
                    this.lblEventTime.Text = String.Format("{0:F}", _event.Time);

                    //Avatar
                    if (_event.LogoID != null)
                    {
                        var picture = this.pictureService.GetPictureById((int)_event.LogoID);
                        if (picture != null)
                        {
                            this.imgAvatar.Source = this.pictureService.ByteToBitmapSource(picture.PictureBinary);
                        }
                    }
                    else
                    {
                        //Avatar
                        var uriSource = new Uri(@"/Exclusive Software;component/Resources/evento-avatar.png", UriKind.Relative);
                        this.imgAvatar.Source = new BitmapImage(uriSource);
                    }

                    this.dgCustomers.ItemsSource = new List<GuestList>();
                    var guestlist = eventService.GetGuestListByEventId(_event.EventID);
                    this.lblGuestCount.Text = guestlist.Count.ToString();
                    this.lblGuestIn.Text = guestlist.Where(x => x.Attended).ToList().Count.ToString();
                }
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.EventAccess, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        private void btnAddGuest_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var ConnectedUser = this.customerService.User;
                if (ConnectedUser != null)
                {
                    CustomerRole cr = this.customerService.GetCustomerRoleByCustomerID(ConnectedUser.CustomerID);
                    if (cr.CustomerRoleID == 3)/// Usuario Basico.
                    {
                        var _event = (Event)this.cbEvent.SelectedItem;
                        if (_event != null)
                        {
                            WindowSpecialAccess wsa = new WindowSpecialAccess(_event, null);
                            wsa.Show();
                        }
                        else
                        {
                            MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("Dede Seleccionar algun evento.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                    else
                    {
                        var _event = (Event)this.cbEvent.SelectedItem;
                        if (_event != null)
                        {
                            CustomerRegister customeRegister = new CustomerRegister(_contentControl);
                            if (_contentControl != null)
                                _contentControl.Content = customeRegister;
                            customeRegister._assistedEvent = _event;
                            customeRegister.BindData(null);
                        }
                        else
                        {
                            MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("Dede Seleccionar algun evento.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.EventAccess, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  

        }

        private void dgCustomers_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                IInputElement element = e.MouseDevice.DirectlyOver;
                if (element != null && element is FrameworkElement)
                {
                    var grid = sender as DataGrid;
                    if (grid != null && grid.SelectedItems != null && grid.SelectedItems.Count == 1)
                    {
                        var _event = (Event)this.cbEvent.SelectedItem;
                        var CustomerListItem = (CustomerListForGuestList)grid.SelectedItem;
                        GetAccessToCustomer(_event, CustomerListItem);
                    }
                }
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.EventAccess, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Search();
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.EventAccess, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        private void SearchKey(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    this.Search();
                    if (this.dgCustomers.Items.Count >= 1)
                    {
                        //dgCustomers.UpdateLayout();
                        //DataGridRow row = dgCustomers.GetRow(0);
                        //row.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                        //row.Focus();
                        //dgCustomers.SelectRowByIndex(0);
                        //Keyboard.Focus(dgCustomers);
                        this.txtSearch.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                    }
                }
                catch (Exception ex)
                {
                    this.logService.InsertLog(LogTypeEnum.EventAccess, ex.Message, ex);
                    MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                } 
            }
        }

        private void dgCustomers_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    var grid = sender as DataGrid;
                    if (grid != null && grid.SelectedItems != null && grid.SelectedItems.Count == 1)
                    {
                        var _event = (Event)this.cbEvent.SelectedItem;
                        var CustomerListItem = (CustomerListForGuestList)grid.SelectedItem;
                        GetAccessToCustomer(_event, CustomerListItem);
                    }
                }
                catch (Exception ex)
                {
                    this.logService.InsertLog(LogTypeEnum.EventAccess, ex.Message, ex);
                    MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }  
            }
        }

        #endregion

        #region Helpers Class

        public class CustomerListForGuestList
        {
            public int CustomerID { get; set; }
            public string CI { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public bool Attended { get; set; }
            public bool Invited { get; set; }
        }

        #endregion

    }
}
