﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Exclusive_Software.Services;
using Exclusive_Software.Model;

namespace Exclusive_Software.Views
{
    /// <summary>
    /// Lógica de interacción para Events.xaml
    /// </summary>
    public partial class Events : UserControl
    {

        ContentControl _contentControl;
        private EventService eventService = new EventService();
        private LogService logService = new LogService();


        public Events(ContentControl contentControl)
        {
            try
            {
                _contentControl = contentControl;
                InitializeComponent();
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.Events, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        public void FillDataGrid(string seachrTerms)
        {
            var eventlist = eventService.GetEventByName(seachrTerms);
            this.dgEvents.ItemsSource = eventlist;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_contentControl != null)
                    _contentControl.Content = new EventRegister(_contentControl);
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.Events, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        private void dgEvents_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                EventRegister eventRegister = new EventRegister(_contentControl);

                IInputElement element = e.MouseDevice.DirectlyOver;
                if (element != null && element is FrameworkElement)
                {
                    var grid = sender as DataGrid;
                    if (grid != null && grid.SelectedItems != null && grid.SelectedItems.Count == 1)
                    {
                        var _event = (Event)grid.SelectedItem;
                        eventRegister.BindData(_event);
                    }
                }

                if (_contentControl != null)
                    _contentControl.Content = eventRegister;
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.Events, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  

        }
    }
}
