﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Exclusive_Software.Model;
using Exclusive_Software.Services;

namespace Exclusive_Software.Views
{
    /// <summary>
    /// Lógica de interacción para PollQuestion.xaml
    /// </summary>
    public partial class PollQuestionView : UserControl
    {
        #region Constants

        ContentControl _contentControl;
        private Poll _poll;
        private PollQuestion _pollQuestion;
        private PollService pollService = new PollService();
        private LogService logService = new LogService();

        #endregion

        #region Ctor

        public PollQuestionView(ContentControl contentControl)
        {
            try
            {
                _contentControl = contentControl;
                InitializeComponent();
                BindData(_pollQuestion, _poll);
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.PollQuestionView, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  
        }

        #endregion

        public void BindData(PollQuestion pollQuestion, Poll poll)
        {
            _poll = poll;
            _pollQuestion = pollQuestion;

            if (_poll != null)
            {
                if (_pollQuestion != null)
                {
                    this.txtQuestion.Text = _pollQuestion.Question;

                    switch (_pollQuestion.AnswerType)
                    {
                        case 1:
                            this.rbSimple.IsChecked = true;
                            break;
                        case 2:
                            this.rbMultiple.IsChecked=true;
                            break;
                    }

                    this.txtAnswer.Text = "";
                    this.iudDisplayOrder.Value = 1;

                    var Answerslist = pollService.GetPollAnswersByPollQuestionID(_pollQuestion.PollQuestionID);
                    this.dgAnswers.ItemsSource = Answerslist;
                }
                else
                {
                    CleanControl();
                }
            }
        }

        public void CleanControl()
        {
            _pollQuestion = new PollQuestion();
            this.txtQuestion.Text="";
            this.txtAnswer.Text="";
            this.rbSimple.IsChecked=true;
            this.iudDisplayOrder.Value=1;

            var Answerslist = pollService.GetPollAnswersByPollQuestionID(_pollQuestion.PollQuestionID);
            this.dgAnswers.ItemsSource = Answerslist;
        
        }

        private void SavePollQuestion()
        {
            bool IsNew = true;

            if (_pollQuestion != null)
            {
                if (_pollQuestion.PollQuestionID != 0)
                {
                    IsNew = false;
                }
            }

            if (IsNew)
            {
                PollQuestion newPollQuestion = new PollQuestion();

                newPollQuestion.Question = this.txtQuestion.Text;
                newPollQuestion.Published = true;

                if (this.rbSimple.IsChecked != null)
                {
                    if ((bool)this.rbSimple.IsChecked)
                    {
                        newPollQuestion.AnswerType = 1;
                    }
                }

                if (this.rbMultiple.IsChecked != null)
                {
                    if ((bool)this.rbMultiple.IsChecked)
                    {
                        newPollQuestion.AnswerType = 2;
                    }
                }

                if (_poll != null)
                {
                    newPollQuestion.Poll_ID = _poll.Poll_ID;
                    pollService.InsertPollQuestion(newPollQuestion);
                    _pollQuestion = newPollQuestion;
                }
            }
            else
            {
                PollQuestion updatePollQuestion = pollService.GetPollQuestionById(_pollQuestion.PollQuestionID);

                updatePollQuestion.Question = this.txtQuestion.Text;
                updatePollQuestion.Published = true;

                if (_poll != null)
                {
                    updatePollQuestion.Poll_ID = _poll.Poll_ID;
                    pollService.UpdatePollQuestion(updatePollQuestion);
                    _pollQuestion = updatePollQuestion;
                }
            }
        }

        private void SavePollAnswer()
        {
            PollAnswer newPollAnswer = new PollAnswer();

            newPollAnswer.Answer = this.txtAnswer.Text;

            if (this.iudDisplayOrder.Value != null)
            {
                newPollAnswer.DisplayOrder = (int)this.iudDisplayOrder.Value;
            }
            else
            {
                newPollAnswer.DisplayOrder = 1;
            }

            if (_poll != null)
            {
                if (_pollQuestion != null)
                {
                    newPollAnswer.PollQuestionID = _pollQuestion.PollQuestionID;
                    pollService.InsertPollAnswer(newPollAnswer);
                }
            }
        }

        private void ReloadControl()
        {
            ManagePoll managePoll = new ManagePoll(_contentControl);
            if (_contentControl != null)
                _contentControl.Content = managePoll;
            managePoll.BindData(_poll);

        }

        private void btnAddAnswer_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.txtQuestion.Text != "")
                {
                    if (this.txtAnswer.Text != "")
                    {
                        if (this.pollService.CanModifyPoll(_poll.Poll_ID))
                        {
                            SavePollQuestion();
                            SavePollAnswer();
                            BindData(_pollQuestion, _poll);
                        }
                        else
                        {
                            MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("La encuesta ya posee respuestas de clientes, no puede ser modificada.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                    else
                    {
                        MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("La respuesta no puede estar vacía.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                else
                {
                    MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("La pregunta no puede estar vacía.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.PollQuestionView, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  

        }

        private void btnDeleteQuestion_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if(this.pollService.CanModifyPoll(_poll.Poll_ID))
                {
                    if (_pollQuestion != null)
                    {
                        pollService.DeletePollQuestion(_pollQuestion.PollQuestionID);
                        ReloadControl();
                    }
                }
                else
                {
                    MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("La encuesta ya posee respuestas de clientes, no puede ser modificada.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                }

            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.PollQuestionView, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            } 
        }

        private void btnDeleteAnswer_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if(this.pollService.CanModifyPoll(_poll.Poll_ID))
                {
                    PollAnswer pa = ((FrameworkElement)sender).DataContext as PollAnswer;
                    if (pa != null)
                    {
                        pollService.DeletePollAnswer(pa.PollAnswerID);
                        ReloadControl();
                    }
                }
                else
                {
                    MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("La encuesta ya posee respuestas de clientes, no puede ser modificada.", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.PollQuestionView, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            } 
        }

    }
}
