//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Exclusive_Software.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Poll
    {
        public Poll()
        {
            this.PollQuestions = new HashSet<PollQuestion>();
        }
    
        public int Poll_ID { get; set; }
        public string PollName { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public System.DateTime UpdatedOn { get; set; }
        public bool Published { get; set; }
        public int EventID { get; set; }
        public bool Deleted { get; set; }
    
        public virtual Event Event { get; set; }
        public virtual ICollection<PollQuestion> PollQuestions { get; set; }
    }
}
