﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Exclusive_Software.Model;
using Exclusive_Software.Services;

namespace Exclusive_Software
{
    /// <summary>
    /// Lógica de interacción para WindowSpecialAccess.xaml
    /// </summary>
    public partial class WindowSpecialAccess : Window
    {
        private Event _assistedEvent;
        private Customer _customer;
        private LogService logService = new LogService();
        private CustomerService customerService = new CustomerService();
        private EventService eventService = new EventService();

        public WindowSpecialAccess(Event assistedEvent, Customer customer)
        {
            InitializeComponent();
            _assistedEvent = assistedEvent;
            _customer = customer;
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Customer customerToLogged = this.customerService.GetCustomerByPassword(this.txtPassword.Password);
                if (customerToLogged != null)
                {
                    CustomerRole cr = this.customerService.GetCustomerRoleByCustomerID(customerToLogged.CustomerID);
                    if (cr != null)
                    {
                        if (cr.CustomerRoleID == 2)
                        {
                            this.Hide();
                            if (_customer != null)
                            {
                                MessageBoxResult result = MessageBox.Show(string.Format("Esta usted seguro que desea Invitar al cliente {0} {1} y registrar su entrada al evento?", _customer.FirstName, _customer.LastName), "Pregunta", MessageBoxButton.YesNo, MessageBoxImage.Question);
                                if (result == MessageBoxResult.Yes)
                                {
                                    var guestList = new GuestList();
                                    guestList.CustomerID = _customer.CustomerID;
                                    guestList.EventID = _assistedEvent.EventID;
                                    guestList.Attended = true;
                                    guestList.AttendedDate = DateTime.Now;
                                    this.eventService.InsertGuestList(guestList);
                                    MessageBoxResult result2 = Xceed.Wpf.Toolkit.MessageBox.Show("El acceso al cliente se ha registrado correctamente", "Información", MessageBoxButton.OK, MessageBoxImage.Information);
                                }
                            }
                            else
                            {
                                foreach (Window window in Application.Current.Windows)
                                {
                                    if (window.GetType() == typeof(MainWindow))
                                    {
                                        MainWindow mw = (MainWindow)window;
                                        mw.BindData(_assistedEvent);
                                        mw.Show();
                                    }
                                }
                            }
                        }
                        else
                        {
                            MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("Contraseña incorrecta.", "Información", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                }
                else
                {
                    MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("Contraseña incorrecta.", "Información", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
                this.logService.InsertLog(LogTypeEnum.EventAccess, ex.Message, ex);
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show(string.Format("Ha Ocurrido un error en el sistema. Favor comunicarse con soporte técnico, el detalle del error es: {0}", ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }  

        }
    }
}
